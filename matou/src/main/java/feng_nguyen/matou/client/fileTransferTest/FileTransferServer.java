package feng_nguyen.matou.client.fileTransferTest;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class FileTransferServer {
	public static void main(String[] args) {
		Selector selector = null;
		ServerSocketChannel serverSocketChannel = null;
		try {
			// Selector for incoming time requests
			selector = Selector.open();
			// Create a new server socket and set to non blocking mode
			serverSocketChannel = ServerSocketChannel.open();
			//serverSocketChannel.configureBlocking(false);
			// Bind the server socket to the local host and port
			//serverSocketChannel.socket().setReuseAddress(true);
			serverSocketChannel.socket().bind(new InetSocketAddress(7777));
			// Register accepts on the server socket with the selector. This
			// step tells the selector that the socket wants to be put on the
			// ready list when accept operations occur, so allowing multiplexed
			// non-blocking I/O to take place.
			//serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
			// Here's where everything happens. The select method will
			// return when any operations registered above have occurred, the
			// thread has been interrupted, etc.
			/*
			while (selector.select() > 0) {
				// Someone is ready for I/O, get the ready keys
				Iterator<SelectionKey> it = selector.selectedKeys().iterator();
				// Walk through the ready keys collection and process date requests.
				while (it.hasNext()) {
					SelectionKey readyKey = it.next();
					it.remove();
					// The key indexes into the selector so you
					// can retrieve the socket that's ready for I/O
					doit((ServerSocketChannel) readyKey.channel());
				}
			}
			*/
			while(!Thread.interrupted()) {
				SocketChannel socketChannel = serverSocketChannel.accept();
				System.out.println("Accept");
				FileTransfer.receiveFile(socketChannel,
						new File("matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/out.zip"));
				System.out.println("Finish");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				//selector.close();
				serverSocketChannel.close();
			} catch (Exception ex) {}
		}
	}

	private static void doit(final ServerSocketChannel serverSocketChannel) throws IOException {
		SocketChannel socketChannel = null;
		try {
			socketChannel = serverSocketChannel.accept();
			FileTransfer.receiveFile(socketChannel,
					new File("matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/out.zip"));
			//FileTransfer.sendFile(socketChannel,
			//		new File("matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/in.zip"));
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				socketChannel.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}

/*
 * public static void main(String[] args) throws Exception { //Initialize
 * Sockets ServerSocket ssock = new ServerSocket(5000); Socket socket =
 * ssock.accept();
 * 
 * //The InetAddress specification InetAddress IA =
 * InetAddress.getByName("localhost");
 * 
 * //Specify the file File file = new
 * File("matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/in.zip");
 * FileInputStream fis = new FileInputStream(file); BufferedInputStream bis =
 * new BufferedInputStream(fis);
 * 
 * //Get socket's output stream OutputStream os = socket.getOutputStream();
 * 
 * //Read File Contents into contents array byte[] contents; long fileLength =
 * file.length(); long current = 0;
 * 
 * // long start = System.nanoTime(); while(current!=fileLength){ int size =
 * 10000; if(fileLength - current >= size) current += size; else{ size =
 * (int)(fileLength - current); current = fileLength; } contents = new
 * byte[size]; bis.read(contents, 0, size); os.write(contents);
 * System.out.println("Sending file ... "+(current*100)/fileLength+"% complete!"
 * ); }
 * 
 * os.flush(); bis.close(); //File transfer done. Close the socket connection!
 * socket.close(); ssock.close(); System.out.println("File sent succesfully!");
 * }
 */
