package feng_nguyen.matou.client.fileTransferTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class FileTransferServer {
	public static void main(String[] args) {
	    Selector selector = null;
	    ServerSocketChannel serverSocketChannel = null;
	    try {
	      // Selector for incoming time requests
	      selector = Selector.open();
	      // Create a new server socket and set to non blocking mode
	      serverSocketChannel = ServerSocketChannel.open();
	      serverSocketChannel.configureBlocking(false);
	      // Bind the server socket to the local host and port
	      serverSocketChannel.socket().setReuseAddress(true);
	      serverSocketChannel.socket().bind(new InetSocketAddress(7777));
	      // Register accepts on the server socket with the selector. This
	      // step tells the selector that the socket wants to be put on the
	      // ready list when accept operations occur, so allowing multiplexed
	      // non-blocking I/O to take place.
	      serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
	      // Here's where everything happens. The select method will
	      // return when any operations registered above have occurred, the
	      // thread has been interrupted, etc.
	      while (selector.select() > 0) {
	        // Someone is ready for I/O, get the ready keys
	        Iterator<SelectionKey> it = selector.selectedKeys().iterator();
	        // Walk through the ready keys collection and process date requests.
	        while (it.hasNext()) {
	          SelectionKey readyKey = it.next();
	          it.remove();
	          // The key indexes into the selector so you
	          // can retrieve the socket that's ready for I/O
	          doit((ServerSocketChannel) readyKey.channel());
	        }
	      }
	    } catch (ClosedChannelException ex) {
	    } catch (IOException ex) {
	    } finally {
	      try {
	        selector.close();
	      } catch(Exception ex) {}
	      try {
	        serverSocketChannel.close();
	      } catch(Exception ex) {}
	    }
	}
	    private static void doit(final ServerSocketChannel serverSocketChannel) throws IOException {
	        SocketChannel socketChannel = null;
	        try {
	          socketChannel = serverSocketChannel.accept();
	          receiveFile(socketChannel, new File("matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/out.txt"));
	          sendFile(socketChannel, new File("matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/in.txt"));
	        } catch(Exception ex){
	        	ex.printStackTrace();
	        }finally {
	          try {
	            socketChannel.close();
	          } catch(Exception ex) {
	        	  	ex.printStackTrace();
	          }
	        }
	      }
	      private static void receiveFile(SocketChannel socketChannel, File file) throws IOException {
	        FileOutputStream fos = null;
	        FileChannel channel = null;
	        try {
	          fos = new FileOutputStream(file);
	          channel = fos.getChannel();
	          ByteBuffer buffer = ByteBuffer.allocateDirect(1024);
	          int size = 0;
	          while ((size = socketChannel.read(buffer)) != -1) {
	            buffer.flip();
	            if (size > 0) {
	              buffer.limit(size);
	              channel.write(buffer);
	              buffer.clear();
	            }
	          }
	        } finally {
	          try {
	            channel.close();
	          } catch(Exception ex) {}
	          try {
	            fos.close();
	          } catch(Exception ex) {}
	        }
	      }
	      private static void sendFile(SocketChannel socketChannel, File file) throws IOException {
	        FileInputStream fis = null;
	        FileChannel channel = null;
	        try {
	          fis = new FileInputStream(file);
	          channel = fis.getChannel();
	          ByteBuffer buffer = ByteBuffer.allocateDirect(1024);
	          int size = 0;
	          while ((size = channel.read(buffer)) != -1) {
	            buffer.rewind();
	            buffer.limit(size);
	            socketChannel.write(buffer);
	            buffer.clear();
	          }
	          socketChannel.socket().shutdownOutput();
	        } finally {
	          try {
	            channel.close();
	          } catch(Exception ex) {}
	          try {
	            fis.close();
	          } catch(Exception ex) {}
	        }
	      }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	public static void main(String[] args) throws Exception {
        //Initialize Sockets
        ServerSocket ssock = new ServerSocket(5000);
        Socket socket = ssock.accept();
        
        //The InetAddress specification
        InetAddress IA = InetAddress.getByName("localhost"); 
        
        //Specify the file
        File file = new File("matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/in.zip");
        FileInputStream fis = new FileInputStream(file);
        BufferedInputStream bis = new BufferedInputStream(fis); 
          
        //Get socket's output stream
        OutputStream os = socket.getOutputStream();
                
        //Read File Contents into contents array 
        byte[] contents;
        long fileLength = file.length(); 
        long current = 0;
         
       // long start = System.nanoTime();
        while(current!=fileLength){ 
            int size = 10000;
            if(fileLength - current >= size)
                current += size;    
            else{ 
                size = (int)(fileLength - current); 
                current = fileLength;
            } 
            contents = new byte[size]; 
            bis.read(contents, 0, size); 
            os.write(contents);
            System.out.println("Sending file ... "+(current*100)/fileLength+"% complete!");
        }   
        
        os.flush(); 
        bis.close();
        //File transfer done. Close the socket connection!
        socket.close();
        ssock.close();
        System.out.println("File sent succesfully!");
    }
    */
}
