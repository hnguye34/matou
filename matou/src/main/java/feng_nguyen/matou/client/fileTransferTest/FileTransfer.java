package feng_nguyen.matou.client.fileTransferTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;

public class FileTransfer {
	public static void sendFile(SocketChannel socketChannel, File file) throws IOException {
		FileInputStream fis = null;
		FileChannel channel = null;
		try {
			fis = new FileInputStream(file);
			channel = fis.getChannel();
			ByteBuffer buffer = ByteBuffer.allocateDirect(Long.BYTES + (int)channel.size());
			//int size = 0;
			//while ((size = channel.read(buffer)) != -1) {
				//buffer.rewind();
				//buffer.limit(size);
				buffer.putLong(channel.size());
				channel.read(buffer);
				buffer.flip();
				socketChannel.write(buffer);
			//	buffer.clear();
			//}
			socketChannel.socket().shutdownOutput();
		} finally {
			try {
				channel.close();
			} catch (Exception ex) {
			}
			try {
				fis.close();
			} catch (Exception ex) {
			}
		}
	}

	public static void receiveFile(SocketChannel socketChannel, File file) throws IOException {
		FileOutputStream fos = null;
		FileChannel channel = null;
		try {
			ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
			if(!readFully(socketChannel,buffer)) {System.out.println("111111");};
			
			fos = new FileOutputStream(file);
			channel = fos.getChannel();
			
			buffer.flip();
			buffer = ByteBuffer.allocate((int)buffer.getLong());
			if(!readFully(socketChannel,buffer)) {System.out.println("222222");};
			//int size = 0;
			//while ((size = socketChannel.read(buffer)) != -1) {
				buffer.flip();
			//	if (size > 0) {
			//		buffer.limit(size);
					channel.write(buffer);
					buffer.clear();
			//	}
			//}
		} finally {
			try {
				channel.close();
			} catch (Exception ex) {
			}
			try {
				fos.close();
			} catch (Exception ex) {
			}
		}
	}
	
	public static boolean readFully(SocketChannel sc, ByteBuffer bb) throws IOException {
		while (bb.hasRemaining()) {
			if (sc.read(bb) == -1) {
				return false;
			}
		}
		return true;
	}
}
