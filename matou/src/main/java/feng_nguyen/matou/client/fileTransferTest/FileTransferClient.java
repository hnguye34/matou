package feng_nguyen.matou.client.fileTransferTest;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;

public class FileTransferClient {

	public static void main(String[] args) throws IOException {
		new Thread( () -> {
			SocketChannel socketChannel = null;
			try {
				socketChannel = SocketChannel.open();
				SocketAddress socketAddress = new InetSocketAddress("localhost", 7777);
				socketChannel.connect(socketAddress);
				FileTransfer.sendFile(socketChannel,
						new File("matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/in.zip"));
				//FileTransfer.receiveFile(socketChannel,
				//		new File("matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/out.zip"));
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				try {
					socketChannel.close();
				} catch (Exception ex) {}
			}
		}).start();
	}

}

/*
 * public static void main(String[] args) throws Exception{
 * 
 * //Initialize socket Socket socket = new
 * Socket(InetAddress.getByName("localhost"), 5000); byte[] contents = new
 * byte[10000];
 * 
 * //Initialize the FileOutputStream to the output file's full path.
 * FileOutputStream fos = new FileOutputStream(
 * "matou/src/main/java/feng_nguyen/matou/client/fileTransferTest/out.zip");
 * BufferedOutputStream bos = new BufferedOutputStream(fos); InputStream is =
 * socket.getInputStream();
 * 
 * //No of bytes read in one read() call int bytesRead = 0;
 * 
 * while((bytesRead=is.read(contents))!=-1) bos.write(contents, 0, bytesRead);
 * 
 * bos.flush(); bos.close(); socket.close();
 * 
 * System.out.println("File saved successfully!"); }
 */
