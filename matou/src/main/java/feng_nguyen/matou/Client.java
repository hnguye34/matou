package feng_nguyen.matou;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

//import org.junit.runner.notification.RunListener.ThreadSafe;

import feng_nguyen.matou.common.LoginStatus;
import feng_nguyen.matou.common.StatusCode;

/**
 * This class includes all the client, inner server and inner client functions.
 * @author Yuheng FENG
 */
public class Client {
	private static final Logger logger = Logger.getLogger(Client.class.getName());
	public static final Charset utf8 = Charset.forName("UTF8");
	public static final Charset ASCII = Charset.forName("ASCII");
	/**
	 * Client's name
	 */
	private String myUserName = null; 
	/**
	 * an authorization id creator
	 */
	private final AuthorizationID authorizationID;
	/**
	 * a flag to determine whether or not to stop the main thread
	 */
	private boolean quit_request;
	/**
	 * Inner server
	 */
	private ServerSocketChannel innerServerSocketChannel = null;
	private int innerServerPort;
	/**
	 * If two or more invitations for private connection come at the same time, they will be stocked here temporary.
	 */
	private List<String> invitations;
	/**
	 * A map to store every private connection. (username + data)
	 */
	private Map<String, InnerClientData> privateConnections;
	/**
	 * Every file received from other user will be put in this folder.
	 */
	private String path_savefile;
	
	public Client() {
		authorizationID = new AuthorizationID();
		quit_request = false;
		invitations = Collections.synchronizedList(new ArrayList<String>());
		privateConnections = Collections
				.synchronizedMap(new HashMap<String, InnerClientData>());
		//path_savefile = "matou/src/main/java/feng_nguyen/matou/Received/";
		path_savefile = "./";
	}
	
	/**
	 * Connect to server, send username until the server accepts the right code.
	 * Then create a listener thread and a sender thread.
	 *
	 * @param args  server address, port, port for inner server 
	 * @throws IOException
	 */
	private void launch(String[] args) throws IOException {
		InetSocketAddress server = new InetSocketAddress(args[0], Integer.valueOf(args[1]));
		innerServerPort = Integer.valueOf(args[2]);
		System.out.println(" *** Welcome to Matou, please type your user name ***");
		try (Scanner scan = new Scanner(System.in); SocketChannel sc = SocketChannel.open(server)) {
			getUsername(scan, sc);
			System.out.println(" *** Type '-help' to show instructions. *** ");
			System.out.println(" *** From now on, you will receive public message automaticlly. *** ");

			/* Listener thread */
			Thread listener = new Thread(() -> {
				ByteBuffer responseStatusCode = ByteBuffer.allocate(Integer.BYTES);
				while (!Thread.interrupted()) {
					responseStatusCode.clear();
					try {
						if (!readFully(sc, responseStatusCode) )
							throw new IOException("Connection lost");
						
						read_response(sc, responseStatusCode);
					} catch (IOException e) {
						System.err.println(" Connection lost. ");
						break;
					}
				}
			});

			listener.start();

			/* Sender Thread */
			for (; !quit_request;) {
				/* When there is an invitation, user must respond and during this time, user can't send public message. */
				/* Waiting for the response of an invitation for private connection */
				if(invitations.size() > 0) {
					System.out.print("\n **** User "+ invitations.get(0) +" would like to open a private connection with you [y/n]:");
					String instr = scan.nextLine();
					if(instr.length() == 0)
						continue;
					if (instr.equals("Y") || instr.equals("y")) {
						/* Private connection : Third step (B -> S) */
						/* After we started the inner server, we got the address and port. So the client only needs to do this once. */
						/*
						for (;innerServerSocketChannel == null;) {				
							System.out.print("\n ***** Type your IP address and port (Example: '123.123.123.123 8080') :");
							addressAndPort = scan.nextLine();
							startInnerServer(addressAndPort);
						}
						*/
						if(innerServerSocketChannel == null)
							startInnerServer();
						//ByteBuffer address = utf8.encode(InetAddress.getLocalHost().getHostAddress());
						ByteBuffer bufGuestId = utf8.encode(invitations.get(0));
						ByteBuffer request = ByteBuffer.allocate(Integer.BYTES * 4 + Long.BYTES + bufGuestId.remaining());
						request.putInt(StatusCode.U_ACV_S.getValue());
						request.putInt(1);
						//request.putInt(address.remaining());
						//request.put(address);
						request.putInt(bufGuestId.remaining());
						request.put(bufGuestId);
						request.putInt(innerServerPort);
						long auId = authorizationID.get();
						request.putLong(auId);
						request.flip();
						//System.out.printf("Finish:%d", auId);
						sc.write(request);
						privateConnections.put(invitations.get(0), new InnerClientData(auId));
						invitations.remove(0);
					} else if (instr.equals("N") || instr.equals("n")) {
						ByteBuffer request = ByteBuffer.allocate(Integer.BYTES * 2);
						request.putInt(StatusCode.U_ACV_S.getValue());
						request.putInt(-1);
						request.flip();
						sc.write(request);
						System.out.println("Finish");
						invitations.remove(0);
					} else
						continue;
				}

				String instr = scan.nextLine();
				if(instr.length() == 0)
					continue;
				logger.info("--->Request:" + instr);
				/* The first character decides the usage of this message. */
				switch (instr.charAt(0)) {
				/* For System function */
				case '-':
					exec_instr(sc, instr.substring(1));
					break;

				/* For private conversation */
				case '@':	
					if(instr.substring(1).split(" ")[0].equals(myUserName)) {
						System.err.println(" *** You needn't send a private message to yourself. *** ");
						break;
					}
					if (!privateConnections.containsKey(instr.substring(1).split(" ")[0])) {
						System.err.println(" **** You need to connect to " + instr.substring(1).split(" ")[0]
								+ " first, then send message. Use '-connect [UUSENAME]' to send request. **** ");
						break;
					}		
					if(instr.substring(1).split(" ").length <= 1)
						break;
					InnerClientData clientData = privateConnections.get(instr.substring(1).split(" ")[0]);
					if(instr.substring(1).split(" ")[1].equals("-send") || instr.substring(1).split(" ")[1].equals("-SEND")) {
						if(instr.substring(1).split(" ").length != 3) {
							System.out.println(" *** Usage: @[USERNAME] -send [PATH_TO_FILE] *** ");
							break;
						}
						String path = instr.substring(1).split(" ")[2];	
						/* Check the path */
						try { 
							FileTransfer.sendFile(clientData.getFileSocketChannel(), new File(path));
						} catch(FileNotFoundException e) {
							System.out.println(" *** Can't find the file. *** ");
							break;
						}
						System.out.println(" *** Sending file - Done *** ");
						break;
					}
					SocketChannel sc_private = clientData.getSocketChannel();
					/* instr = 								"@FENG hello world toto titi"
					 * instr.substring(1) = 					"FENG hello world toto titi"
					 * instr.substring(1).split(" ", 2)[1] = "hello world toto titi" */
					ByteBuffer message = utf8.encode(instr.substring(1).split(" ", 2)[1]);
					ByteBuffer request = ByteBuffer.allocate(Integer.BYTES * 2 + message.remaining());
					request.putInt(StatusCode.U_MSG_U.getValue());
					request.putInt(message.remaining());
					request.put(message);
					request.flip();
					sc_private.write(request);
					break;

				/* For message public */
				default:
					logger.info("--->envoi:" + instr);
					message = utf8.encode(instr);
					request = ByteBuffer.allocate(Integer.BYTES * 2 + message.remaining());
					request.putInt(StatusCode.U_MSG_S.getValue());
					request.putInt(message.remaining());
					request.put(message);
					request.flip();
					sc.write(request);
				}

			}
		}
		System.out.println(" *** Log out successfully. Thank you for using Matou. *** ");
	}
/*
	private static boolean IPAddressValid(String addressAndPort) {
		if (addressAndPort.split(" ").length != 2)
			return false;
		String[] address = addressAndPort.split(" ")[0].split(".");
		int port = Integer.parseInt(addressAndPort.split(" ")[1]);

		if (address.length != 4)
			return false;
		if (port <= 0 || port > 65535)
			return false;

		for (String i : address)
			if (Integer.parseInt(i) > 255 || Integer.parseInt(i) < 1)
				return false;

		return true;
	}
*/
	/**
	 * send username until the server accepts the right code.
	 *
	 * @param scan  user input
	 * @param sc    socket to the server
	 */
	private void getUsername(Scanner scan, SocketChannel sc) throws IOException {
		/* Ask for username until the server returns 1 */
		for (;;) {
			/* send username */
			myUserName = scan.nextLine();
			if(myUserName.length() == 0)
				continue;
			ByteBuffer login = utf8.encode(myUserName);
			ByteBuffer request = ByteBuffer.allocate(Integer.BYTES * 2 + login.remaining());
			request.putInt(StatusCode.U_LOG_S.getValue());
			//int len = login.remaining();
			request.putInt(login.remaining());
		//	System.out.printf("Login=%s. Len=%d", line, len);
			request.put(login);
			request.flip();
			sc.write(request);

			/* Read response */
			ByteBuffer response = ByteBuffer.allocate(Integer.BYTES * 2);
			do {
				if (!readFully(sc, response))
					throw new IOException("Connection lost");
			}while (!StatusCode.from(response).equals(StatusCode.S_LOG_U));
				//throw new IllegalArgumentException(" Status Code Error. ");

			if (S_LOG_U(response.getInt()))
				break;

		}
	}	
	
	/**
	 * To create a inner server.
	 * If user replies Yes to an invitation of private connection (only in first time), this function will be called.
	 */
	private void startInnerServer() throws IOException {	
		innerServerSocketChannel = ServerSocketChannel.open();
		innerServerSocketChannel.bind(new InetSocketAddress(innerServerPort));
		System.out.println("Inner bind on "+innerServerPort);
		Thread innerServer = new Thread(()->{
			while(!Thread.interrupted()) {
                final SocketChannel innerMessageServer;
                final SocketChannel innerFileServer;
                try {
                		innerMessageServer = innerServerSocketChannel.accept();
                		System.out.println("Inner message server accept !");
                		innerFileServer = innerServerSocketChannel.accept();
                    System.out.println("Inner file server accept !");
                    ByteBuffer U_RLG_U = ByteBuffer.allocate(Integer.BYTES *2);
                    U_RLG_U.putInt(StatusCode.U_RLG_U.getValue());
                    
                    String otherUsername = registerSocketChannel(innerMessageServer, U_RLG_U);
                    String otherUsername2 = registerSocketChannel(innerFileServer, U_RLG_U);
                    
                    if(otherUsername == null || otherUsername2 == null) {
                    		System.out.println(" Server doesn't receive the right request. ");
                    		break;
                    }
                    if( !otherUsername.equals(otherUsername2)) {
                    		System.out.println(" Not thread safe ");
                    		break;
                    }
                    
                    System.out.println("Authorization ID match! Start to listen ");
                    privateConnections.get(otherUsername).setMessageChannel(innerMessageServer);
                    privateConnections.get(otherUsername).setFileChannel(innerFileServer);
                    
                    /* Private connection: Sixth step  (B -> A), sending back U_RLG_U */
                    U_RLG_U.putInt(1);
                    U_RLG_U.flip();
                    innerMessageServer.write(U_RLG_U);
                    U_RLG_U.rewind();
                    innerFileServer.write(U_RLG_U);
                    
                    createPrivateConnectionListener(innerMessageServer, otherUsername);
                    createPrivateConnectionFileListener(innerFileServer, otherUsername);

                    
                } catch (IOException ioe) {
                    logger.log(Level.INFO,"Connection terminated with client by IOException",ioe.getCause());
                    return;
                }
            }
		}); 
		innerServer.start();
	}
	
	/**
	 * Read the username from the socket and return
	 *
	 * @param innerServer	socket of the inner server
	 * @param U_RLG_U		Bytebuffer for response code U_RLG_U
	 * @return 
	 *  - null if private connection doesn't be created successfully.
	 *  - username if private connection has been created.
	 * @throws IOException
	 */
	private String registerSocketChannel(final SocketChannel innerServer, ByteBuffer U_RLG_U) throws IOException {
		ByteBuffer intBuffer = ByteBuffer.allocate(Integer.BYTES);
		if(!readFully(innerServer,intBuffer)) {
				logger.info("Private connection lost");
				return null;
		}
		//System.out.println("Inner has read U_LOG_U !");
		if(StatusCode.from(intBuffer)!=StatusCode.U_LOG_U) {
				logger.info("Error status code in private connection.");
				return null;
		}
		String otherUsername = readAnIntThenString(innerServer,ASCII);
		//System.out.println("Inner has read login !");
		ByteBuffer longBuffer = ByteBuffer.allocate(Long.BYTES);
		if(!readFully(innerServer,longBuffer)) {
				logger.info("Private connection lost");
				return null;
		}
		longBuffer.flip();
		//System.out.println("Inner has read authorization id !");
		long authorizationID = longBuffer.getLong();
		if(!privateConnections.containsKey(otherUsername)) {
				logger.info("Unknown user");
				return null;
		}
		//System.out.println(otherUsername+" ====== "+authorizationID);
		if(!privateConnections.get(otherUsername).checkID(authorizationID)) {
		    	logger.info("AuthorizationID doesn't match!");
		    	privateConnections.remove(otherUsername);
		    	U_RLG_U.putInt(-2);
		    U_RLG_U.flip();
		    innerServer.write(U_RLG_U);
			return null;
		}
		return otherUsername;
	}

	/**
	 * Message listener for private connection.
	 *
	 * @param innerClient
	 * @param otherUsername	
	 */
	private void createPrivateConnectionListener(final SocketChannel innerClient, String otherUsername) {		
		Thread privateConnectionListerner = new Thread(() -> {
			boolean disconnected = false;
			System.out.println(" **** Private connection with " + otherUsername + " is created. **** ");
			ByteBuffer privateResponseStatusCode = ByteBuffer.allocate(Integer.BYTES);
			while (!Thread.interrupted() && !disconnected) {
				privateResponseStatusCode.clear();
				try {
					if(!readFully(innerClient, privateResponseStatusCode)) {
						System.err.println(" Private connection lost. ");
						disconnected = true;
						privateConnections.remove(otherUsername); 
						return;
					}
					// Other user send a private message
					privateResponseStatusCode.flip();
					switch(StatusCode.from(privateResponseStatusCode)) {
					case U_MSG_U:
						String privateMessage = readAnIntThenString(innerClient, utf8);
						System.out.println("( Message private from " + otherUsername + " ) : " + privateMessage);
						break;
					case U_DIS_U:
						System.out.println(" Disconnect "+otherUsername);
						innerClient.close();	
						privateConnections.remove(otherUsername); 
						disconnected = true;
						break;
					default:
							logger.info("Private connection return value error.");
					}
				} catch (IOException e) {
					System.err.println(" Private connection lost. ");
					disconnected = true;
					privateConnections.remove(otherUsername); 
					break;
				}

			}
		});
		privateConnectionListerner.start();
	}
	
	/**
	 * File listener for private connection.
	 *
	 * @param innerClient
	 * @param otherUsername	
	 */
	private void createPrivateConnectionFileListener(final SocketChannel innerClient, String otherUsername) {
		Thread privateConnectionFileListerner = new Thread(() -> {
			boolean disconnected = false;
			System.out.println(" **** Private connection (file) with " + otherUsername + " is created. **** ");
			ByteBuffer privateResponseStatusCode = ByteBuffer.allocate(Integer.BYTES);
			while (!Thread.interrupted() && !disconnected) {
				privateResponseStatusCode.clear();
				try {
					if(!readFully(innerClient, privateResponseStatusCode)) {
						System.err.println(" Private connection lost. ");
						disconnected = true;
						privateConnections.remove(otherUsername); 
						return;
					}
					privateResponseStatusCode.flip();
					switch(StatusCode.from(privateResponseStatusCode)) {
					case U_FIL_U:
						String fileName = readAnIntThenString(innerClient,ASCII);
						String path_to_download;
						if(path_savefile.endsWith("/")) {
							path_to_download = path_savefile + fileName;
						}
						else 
							path_to_download = path_savefile + "/" + fileName;
						FileTransfer.receiveFile(innerClient, new File(path_to_download));
						System.out.println(" *** File received : "+fileName +" . *** ");
						break;
					case U_DIS_U:
						System.out.println(" Disconnect "+otherUsername);
						innerClient.close();	
						privateConnections.remove(otherUsername); 
						disconnected = true;
						break;
					default:
							logger.info("Private connection return value error.");
					}
				} catch (IOException e) {
					System.err.println(" Private connection lost. ");
					disconnected = true;
					privateConnections.remove(otherUsername); 
					break;
				}

			} 
		});
		privateConnectionFileListerner.start();
	}
	
	private boolean readFully(SocketChannel sc, ByteBuffer bb) throws IOException {
		while (bb.hasRemaining()) {
			if (sc.read(bb) == -1) {
				logger.info("Input stream closed");
				return false;
			}
		}
		return true;
	}

	private boolean S_LOG_U(int response) {
		LoginStatus loginStatus = LoginStatus.from(response).orElseThrow( () -> new IllegalArgumentException(" Server return error: " + response));
		System.out.println(loginStatus.toString());
		return loginStatus == LoginStatus.OK;
	}
	
	/**
	 * System functions
	 *
	 * @param sc			socket of the server
	 * @param command	user input
	 * @throws IOException
	 */
	private void exec_instr(SocketChannel sc, String command) throws IOException {
		ByteBuffer request;
		String[] param = command.split(" ");
		assert param.length > 0 ;
		String instr = param[0];
		
		switch (instr) {
		case "quit":
		case "QUIT":
			request = ByteBuffer.allocate(Integer.BYTES);
			request.putInt(StatusCode.U_DIS_S.getValue());
			request.flip();
			sc.write(request);
			quit_request = true;
			break;

		case "list":
		case "LIST":
			request = ByteBuffer.allocate(Integer.BYTES);
			request.putInt(StatusCode.U_LIS_S.getValue());
			request.flip();
			sc.write(request);
			break;
		
		case "private":
		case "PRIVATE":
			if(privateConnections.size() == 0)
				System.out.println(" *** You haven't connect to anyone yet. *** ");
			else {
				System.out.println("**** "+ privateConnections.size()+ " users has connected to you: ****** ");
				for (String name : privateConnections.keySet())
					System.out.println("**  " + name);
				System.out.println("***************************************** ");
			}
			break;
		
		/* Private connection : First step (A -> S) */
		case "connect":
		case "CONNECT":
			
			if (param.length != 2) {
				System.err.println(" *** Usage: -connect [USERNAME] : Ask for a private connection to [USERNAME]. *** ");
			}
			if(param[1].equals(myUserName)) {
				System.err.println(" *** You can't create a private connection to yourself. *** ");
				break;
			}
			if(privateConnections.containsKey(param[1])) {
				System.err.println(" *** You are already connected to "+param[1]+" ! *** ");
				break;
			}
			ByteBuffer anotherUsername = utf8.encode(param[1]);
			request = ByteBuffer.allocate(Integer.BYTES * 2 + anotherUsername.remaining());
			request.putInt(StatusCode.U_COV_S.getValue());
			request.putInt(anotherUsername.remaining());
			request.put(anotherUsername);
			request.flip();
			sc.write(request);
			break;
			
		case "disconnect":
		case "DISCONNECT":
			if (param.length != 2) {
				System.err.println(" *** Usage: -disconnect [USERNAME] : to disconnect [USERNAME]. *** ");
			}
			if (!privateConnections.containsKey(param[1])) {
				System.err.println(" **** You haven't connected to " + param[1] + " . **** ");
				break;
			}
			if(param[1].equals(myUserName)) {
				System.err.println(" *** You needn't disconnect yourself. If you want to log out, type '-quit'. *** ");
				break;
			}
			InnerClientData clientData = privateConnections.get(param[1]);
			SocketChannel sc_private_message = clientData.getSocketChannel();
			SocketChannel sc_private_file = clientData.getFileSocketChannel();
			
			request = ByteBuffer.allocate(Integer.BYTES);
			request.putInt(StatusCode.U_DIS_U.getValue());
			request.flip();
			sc_private_message.write(request);
			request.rewind();
			sc_private_file.write(request);
			
			clientData.close();
			privateConnections.remove(param[1]); 
			System.out.println(" *** Disconnect " + param[1] + " success. *** ");
			break;
		
		case "path":
		case "PATH":
			if (param.length != 2) {
				System.err.println(" *** -path [PATH] : Files received from other users will be saved to [PATH] *** ");
			}
			File file = new File(param[1]);
			if(file.isFile()) {
				System.err.println(" *** You need to use a folder to save files. *** ");
				break;
			}
			if(file.isDirectory()) {
				path_savefile = param[1];
				System.out.println(" *** Path has been changed to " + param[1] + " . *** ");
			}
			else
				System.err.println(" *** Folder doesn't exist. *** ");
			break;
			
		case "help":
		case "HELP":
			usage();
			break;

		default:
			System.err.printf(" *** Unknown instruction. Use '-help' to find the right instruction:'%s' ***\n", instr);
		}
	}
	
	/**
	 * Read the response from the server.
	 *
	 * @param sc						socket of the server
	 * @param responseStatusCode		ByteBuffer of the server's response
	 * @throws IOException
	 */
	private void read_response(SocketChannel sc, ByteBuffer responseStatusCode) throws IOException {
		logger.info("read_response: begin");
		responseStatusCode.flip();
		StatusCode reponseCode = StatusCode.from(responseStatusCode);
		if (reponseCode == null) {
			logger.warning(" Status Code Error. ");
			return;
		}
		switch (reponseCode) {
		/* user receive a public message */
		case S_MSG_U:
			String author = readAnIntThenString(sc, utf8);
			String message = readAnIntThenString(sc, utf8);
			System.out.println(author + " : " + message);
			break;

		/* user receive a list of username */
		case S_LIS_U:
			String[] users = readAnIntThenString(sc, utf8).split(" ");
			System.out.println("****** Online Users: ****** ");
			for (String name : users)
				System.out.println("* " + name);
			System.out.println("*************************** ");
			break;

		/* user receive a notification of disconnection */
		case S_DIS_U:
			System.out.println("** User " + readAnIntThenString(sc, utf8) + " is offline. ** ");
			break;

		/* Private connection : Fourth step (S -> A) */
		/* user receive the response of the request of private connection */
		case S_COV_U:
			/*
			if (privateConnections.size() > maxPrivateConnections) {
				System.err.println(" Too many private connection at the same time : " + maxPrivateConnections);
				break;
			}
			*/
			System.out.println(" *** Received the private connection response. *** ");
			ByteBuffer intbuffer = ByteBuffer.allocate(Integer.BYTES);
			ByteBuffer longbuffer = ByteBuffer.allocate(Long.BYTES);

			if (!readFully(sc, intbuffer))
				throw new IOException("Connection lost");
			intbuffer.flip();
			int responseCode = intbuffer.getInt();
			if (responseCode == -1) {
				System.out.println("User refuse private connection. ");
				break;
			} 

			String username = readAnIntThenString(sc,ASCII);
			String address = readAnIntThenString(sc,utf8);

			/*intbuffer.clear();
			if (!readFully(sc, intbuffer))
				throw new IOException("Connection lost");
			intbuffer.flip();
			int port = intbuffer.getInt();*/
			
			intbuffer.clear();
			if (!readFully(sc, intbuffer))
				throw new IOException("Connection lost");
			intbuffer.flip();
			int exhangeFilePort = intbuffer.getInt();
			
			if (!readFully(sc, longbuffer))
				throw new IOException("Connection lost");
			
			longbuffer.flip();
			long autorizationID = longbuffer.getLong();
			/*System.out.printf("You have received the reponse from '%s' for your private invitation: authorisation Id='%d'. IP='%s'. Port for file exchange = '%d'.  \n",
					username, autorizationID, address,   exhangeFilePort);*/
			
			/* Private connection : Fifth step (A -> B) */
			InetSocketAddress privateConnectAddress = new InetSocketAddress(address, exhangeFilePort);
			//System.out.println(" *** Inetsocketaddress *** ");
			SocketChannel sc_private_message = SocketChannel.open(privateConnectAddress);
			SocketChannel sc_private_file = SocketChannel.open(privateConnectAddress);
			System.out.println(" *** Connect to "+username+" success! *** ");
			privateConnections.put(username, new InnerClientData(autorizationID, sc_private_message, sc_private_file));
			/* Send U_LOG_U */
			ByteBuffer login = ASCII.encode(myUserName);
			ByteBuffer request = ByteBuffer.allocate(Integer.BYTES * 2 + Long.BYTES + login.remaining());
			request.putInt(StatusCode.U_LOG_U.getValue());
			request.putInt(login.remaining());
			request.put(login);
			request.putLong(autorizationID);
			request.flip();
			sc_private_message.write(request);
			request.rewind();
			sc_private_file.write(request);
			System.out.println(" *** Send U_LOG_U successfully *** ");
			/* Wait for U_RLG_U */
			
			if(!checkResponseForCreatingPrivateConnection(sc_private_message,username) || 
					!checkResponseForCreatingPrivateConnection(sc_private_file,username)) {
				break;
			}
			
			/*
			 * Private connection is created successfully. 
			 * Create a listener thread for this private Connection.
			 */
			createPrivateConnectionListener(sc_private_message, username);
			createPrivateConnectionFileListener(sc_private_file, username);
			break;
			
			/* Private connection : Second step (S -> B) */
			/* user receive the invitation of private connection */
		case S_TCV_U:
			String anotherUser = readAnIntThenString(sc, utf8);
			System.out.println(" *** You have received a private connection invitation, press enter to response. *** ");
			invitations.add(anotherUser);
			break;

		default:
			logger.warning(String.format("-->read_response: Status Code Error:%d", reponseCode.getValue()));
			// throw new IllegalArgumentException(" Status Code Error. ");
		}
	}
	/**
	 * Check whether the response of the invitation of a private connection is correct.
	 *
	 * @param innerClient	socket of the inner server
	 * @param username		ByteBuffer for response code U_RLG_U
	 * @return boolean : whether the user is the right person to connect.
	 * @throws IOException
	 */
	private boolean checkResponseForCreatingPrivateConnection(SocketChannel innerClient, String username) throws IOException {
		ByteBuffer response = ByteBuffer.allocate(Integer.BYTES * 2);
		if (!readFully(innerClient, response)) {
			logger.info("Private connection lost");
			return false;
		}
		response.flip();
		int responseCode = response.getInt();
		if (responseCode != StatusCode.U_RLG_U.getValue()) {
			logger.info("Private connection return value error : " + responseCode);
			return false;
		}
		switch (response.getInt()) {
		case 1:
			return true;
		case -2:
			System.err.println("Wrong parameters. ");
			privateConnections.remove(username); 
			return false;
		case -3:
			System.err.println("That client reaches max private connections. Please wait a few minutes... ");
			privateConnections.remove(username); 
			return false;
		default:
			throw new IllegalArgumentException("Return value error. ");
		}
	}
	
	/**
	 * Read the size of a string and then read this string.
	 *
	 * @param sc
	 * @param cs	
	 * @return String
	 * @throws IOException
	 */
	private String readAnIntThenString(SocketChannel sc, Charset cs) throws IOException {
		ByteBuffer intbuffer = ByteBuffer.allocate(Integer.BYTES);
		ByteBuffer response;

		intbuffer.clear();
		if (!readFully(sc, intbuffer))
			throw new IOException("Connection lost");
		intbuffer.flip();

		response = ByteBuffer.allocate(intbuffer.getInt());
		if (!readFully(sc, response))
			throw new IOException("Connection lost");
		response.flip();

		return cs.decode(response).toString();
	}
	
	private static void usage() {
		System.out.println("*****************************************************************");
		System.out.println("* [MESSAGE]: Send a private message to everyone online. ");
		System.out.println("* -list : Ask for a list of all online users' name. ");
		System.out.println("* -quit : Deconnection and quit Matou. ");
		System.out.println("* -connect [USERNAME] : Ask for a private connection to [USERNAME]. ");
		System.out.println("* -disconnect [USERNAME] : Stop the private connection of [USERNAME]. ");
		System.out.println("* -private : Ask for a list of users' name that you have connected to them in private. ");
		System.out.println("* @[USERNAME] [MESSAGE]: Send a private message to [USERNAME] (need connect first). ");
		System.out.println("* @[USERNAME] -send [PATH_TO_FILE]: Send a file to [USERNAME] (need connect first). ");
		System.out.println("* -path [PATH] : Files received from other users will be saved to [PATH] ");
		System.out.println("*****************************************************************");
	}
	
	
	public static void main(String[] args) throws IOException {
		if(args.length != 3) {	
			System.out.println(" *** Usage: Client Address Port Port2 *** ");
			System.out.println(" *** [Address Port] for connect to the server. *** ");
			System.out.println(" *** [Port2] for launce a inner server for the private connection. *** ");
			return;
		}
		Client client = new Client(); // max connection
		logger.setLevel(Level.WARNING);
		client.launch(args);
	}
}