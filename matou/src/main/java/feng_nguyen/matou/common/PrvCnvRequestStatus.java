package feng_nguyen.matou.common;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * This enum enumerates all possible values in the response to a private invitation request of the user
 *
 */
public enum PrvCnvRequestStatus {

	OK (1,  "Private conversation accepted"), ERR_REFUSED(-1, "Private conversation refused.") ;

	private final int value;
	private final String desc;
	
	private PrvCnvRequestStatus(int value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	@Override
	public String toString() {
		return this.desc;
	}
	public int toInt() {
		return value;
	}
	
	private static final Map<Integer, PrvCnvRequestStatus> map = new HashMap<Integer, PrvCnvRequestStatus>();
	static {
		for (PrvCnvRequestStatus code : PrvCnvRequestStatus.values())
			map.put(code.value, code);
	}

	public static PrvCnvRequestStatus from(int code) {
		PrvCnvRequestStatus status =  map.get(code);
		
		if (status == null) 	throw  new IllegalStateException (String.format("The login reponse code unknown: %d.", code));
		return status;
	}
	
	public int value() {
		return value;
	}

	/*
	 * The input buffer must be in read mode.
	 */
	public static PrvCnvRequestStatus from(ByteBuffer bufLoginResponsetId) {
		int code = bufLoginResponsetId.getInt();
		return  PrvCnvRequestStatus.from(code);
	}
}
