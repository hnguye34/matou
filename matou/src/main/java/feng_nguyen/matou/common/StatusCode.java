package feng_nguyen.matou.common;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 
 * This enum enumerates all the request type used in the communication between the server and the user
 *
 */
public enum StatusCode {
	
	U_LOG_S(101), S_LOG_U(201), U_MSG_S(102), S_MSG_U(202), U_LIS_S(103), S_LIS_U(203), S_DIS_U(212), S_TCV_U(
			204), U_ACV_S(105), S_COV_U(205),
	
	U_DIS_S(112),U_COV_S(104),U_LOG_U(106),U_RLG_U(107),U_MSG_U(108),U_FIL_U(109),U_RFL_U(110),U_DIS_U(111);

	private final int value;
//	private static final Logger logger = Logger.getLogger(StatusCode.class.getName());
	// Mapping difficulty to difficulty id
	private static final Map<Integer, StatusCode> map = new HashMap<Integer, StatusCode>();
	static {
		for (StatusCode code : StatusCode.values())
			map.put(code.value, code);
	}

	StatusCode( int newValue) {
		value = newValue;
	}

	private static Optional<StatusCode> from(int code) {
		return Optional.ofNullable(map.get(code));
	}
	
	public int getValue() {
		return value;
	}

	public static StatusCode from(ByteBuffer bufResquestId) {
		if ( bufResquestId.position() != 0 )
			bufResquestId.flip();
		
		int code = bufResquestId.getInt();
		Optional<StatusCode> statusCode = StatusCode.from(code);
		return statusCode.orElseThrow(() -> new IllegalStateException (String.format("Request code unknown: %d.", code) ) ) ;
	}

}
