package feng_nguyen.matou.common;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * This enum enumerates all possible values in the response to a login request of the user
 *
 */
public enum LoginStatus {

	OK(1,  "*** Connected successfully. ***"), ERR_TOOLONG(-5, "Username too long, up to 20 caracters"), 
	ERR_CHAR_FORBID(-4,"You cannot use space in the username"), ERR_ALREADY_USED(-2,"Username already exist! Please use another name.");
	public final static String SEPARATOR = " "; 
	private final int value;
	private final String desc;
	
	private LoginStatus(int value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	@Override
	public String toString() {
		return this.desc;
	}
	public int toInt() {
		return value;
	}
	
	private static final Map<Integer, LoginStatus> map = new HashMap<Integer, LoginStatus>();
	static {
		for (LoginStatus code : LoginStatus.values())
			map.put(code.value, code);
	}

	public static Optional<LoginStatus> from(int code) {
		return Optional.ofNullable(map.get(code));
	}
	
	public int value() {
		return value;
	}

	/*
	 * The input buffer must be in read mode.
	 */
	public static LoginStatus from(ByteBuffer bufLoginResponsetId) {
		int code = bufLoginResponsetId.getInt();
		Optional<LoginStatus> loginStatus = LoginStatus.from(code);
		return loginStatus.orElseThrow(() -> new IllegalStateException (String.format("The login reponse code unknown: %d.", code) ) ) ;
	}
}
