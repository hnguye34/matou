package feng_nguyen.matou;

import java.io.IOException;
import java.nio.channels.SocketChannel;

/**
 * Data of each client in private connection
 * @author Yuheng FENG
 */
public class InnerClientData {
	/**
	 * Unique authorization id
	 */
	private final long authorizationID;
	/**
	 * socket channel for transferring private messages
	 */
	private SocketChannel sc;
	/**
	 * socket channel for transferring files
	 */
	private SocketChannel file_sc;
	
	public InnerClientData(long authorizationID, SocketChannel sc, SocketChannel file_sc) {
		this.authorizationID = authorizationID;
		this.sc = sc;
		this.file_sc = file_sc;
	}
	
	public InnerClientData(long authorizationID) {
		this.authorizationID = authorizationID;
	}
	
	public SocketChannel getSocketChannel() {
		return sc;
	}
	
	public SocketChannel getFileSocketChannel() {
		return file_sc;
	}
	
	public void close() {
		try {
			sc.close();
			file_sc.close();
			//listener.interrupt();
		} catch (IOException | NullPointerException e) {
		}
		
	}
	/*
	public void setListener(Thread listener) {
		this.listener = listener;
		listener.start();
	}
	*/
	public boolean checkID(long authorizationID) {
		return this.authorizationID == authorizationID;
	}
	
	public void setMessageChannel(SocketChannel sc) {
		this.sc = sc;
	}
	
	public void setFileChannel(SocketChannel file_sc) {
		this.file_sc = file_sc;
	}

	
	
}
