package feng_nguyen.matou;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SocketChannel;

import feng_nguyen.matou.common.StatusCode;

/**
 * For sending and receiving file via a socket.
 * @author Yuheng FENG
 */
public class FileTransfer {
	public static void sendFile(SocketChannel socketChannel, File file) throws IOException {
		FileInputStream fis = null;
		FileChannel channel = null;
		String fileName = file.getName();
		ByteBuffer name = Client.ASCII.encode(fileName);
		try {
			/* send header */
			fis = new FileInputStream(file);
			channel = fis.getChannel();
			ByteBuffer buffer = ByteBuffer.allocate((int)channel.size() + Integer.BYTES * 2 + name.remaining() + Long.BYTES);
			buffer.putInt(StatusCode.U_FIL_U.getValue());
			buffer.putInt(name.remaining());
			buffer.put(name);
			buffer.putLong(channel.size());
			/* send file */
			channel.read(buffer);
			buffer.flip();
			socketChannel.write(buffer);
		} finally {
			try {
				channel.close();
			} catch (Exception ex) {
			}
			try {
				fis.close();
			} catch (Exception ex) {
			}
		}
	}

	public static void receiveFile(SocketChannel socketChannel, File file) throws IOException {
		FileOutputStream fos = null;
		FileChannel channel = null;
		try {
			fos = new FileOutputStream(file);
			channel = fos.getChannel();
			ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
			if(!readFully(socketChannel,buffer)){
				System.out.println(" Private file connection lost. ");
			}
			buffer.flip();
			long fileSize = buffer.getLong();
			buffer = ByteBuffer.allocate((int)fileSize);
			
			if(!readFully(socketChannel,buffer)){
				System.out.println(" Private file connection lost. ");
			}
			buffer.flip();
			channel.write(buffer);
		} finally {
			try {
				channel.close();
			} catch (Exception ex) {
			}
			try {
				fos.close();
			} catch (Exception ex) {
			}
		}
	}
	
	private static boolean readFully(SocketChannel sc, ByteBuffer bb) throws IOException {
		while (bb.hasRemaining()) {
			if (sc.read(bb) == -1) {
				return false;
			}
		}
		return true;
	}
}
