package feng_nguyen.matou.server;

import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.inrequestreader.RequestReader;
import feng_nguyen.matou.server.outrequestbuilder.NotifyUserBuilder;
import feng_nguyen.matou.server.outrequestbuilder.OutRequestManager;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The class server : provides two distinct interfaces for the security reason.
 *
 */
class Server implements IServer, IServerAdmin {

	static private class Context implements IUserContext {

		private enum State {
			WAITING_IDENTIFICATION, REGISTERED, CLOSED
		};

		private State state = State.WAITING_IDENTIFICATION;
		private boolean activeSinceLastTimeoutCheck = false;
		private final SelectionKey key;
		private final SocketChannel sc;
		private final ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
		private final ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
		/**
		 * the queue to store all message to send.
		 */
		private final LinkedList<ByteBuffer> toSendMsgs = new LinkedList<>();
		private String login = "";
		/**
		 * the message reader
		 */
		private final RequestReader messageReader;
		private final Server server;

		private Context(Server server, SelectionKey key, IObserver observer) {
			this.key = key;
			this.sc = (SocketChannel) key.channel();
			this.server = server;
			messageReader = new RequestReader(this.bbin, this, observer);
		}

		/**
		 * do adequate action for the the content of bbin into bbout
		 *
		 * The convention is that both buffers are in write-mode before the call to
		 * process and after the call
		 *
		 */

		private void processIn() {
			this.messageReader.process();
			updateInterestOps();

		}

		/**
		 * Add a message to the message queue, tries to fill bbOut and updateInterestOps
		 *
		 * @param msg : message to send
		 */
		@Override
		public void send(ByteBuffer msg) {
			if (this.toSendMsgs.size() > this.server.MAX_QUEUE_SIZE || this.state != State.REGISTERED)
				return;

			this.toSendMsgs.add(msg);
			processOut();

		}

		/**
		 * Try to fill bbout from the message queue
		 *
		 */
		private void processOut() {
			
			if (this.toSendMsgs.size() > 0 && this.bbout.hasRemaining()) {

				ByteBuffer first = this.toSendMsgs.peekFirst();

				if (first.remaining() <= this.bbout.remaining()) {
					this.bbout.put(first);
					this.toSendMsgs.removeFirst();
				}
			}
			updateInterestOps();
		}

		/**
		 * Update the interestOps of the key looking only at values of the boolean
		 * closed and of both ByteBuffers.
		 *
		 * The convention is that both buffers are in write-mode before the call to
		 * updateInterestOps and after the call. Also it is assumed that process has
		 * been be called just before updateInterestOps.
		 */

		private void updateInterestOps() {

			int newInterestOp = 0;

			if (this.bbout.position() != 0 || this.toSendMsgs.size() > 0)
				newInterestOp |= SelectionKey.OP_WRITE;

			// if it remains place for new incomming message
			if (this.state != State.CLOSED && this.bbin.hasRemaining())
				newInterestOp |= SelectionKey.OP_READ;

			if (newInterestOp == 0)
				this.silentlyClose();
			else
				this.key.interestOps(newInterestOp);
		}

		private void silentlyClose() {
			try {
				this.state = State.CLOSED;
				sc.close();
				logger.info(String.format("--> Disconnecting user '%s' from:%s:%d", !login.isEmpty() ? login : "",
						this.sc.socket().getInetAddress().toString(), this.sc.socket().getPort()));
			} catch (IOException e) {
				// ignore exception
			}
		}

		/**
		 * Performs the read action on sc
		 *
		 * The convention is that both buffers are in write-mode before the call to
		 * doRead and after the call
		 *
		 * @throws IOException: 
		 * connection error
		 */

		private void doRead() throws IOException {

			if (this.sc.read(this.bbin) == -1) {
				this.state = State.CLOSED;
			}
			this.activeSinceLastTimeoutCheck = true;
			this.processIn();
		}

		/**
		 * Performs the write action on sc
		 *
		 * The convention is that both buffers are in write-mode before the call to
		 * doWrite and after the call
		 *
		 * @throws IOException : connection error
		 */

		private void doWrite() throws IOException {
			bbout.flip();
			this.sc.write(this.bbout);
			this.activeSinceLastTimeoutCheck = true;
			this.bbout.compact();
			this.processOut();

		}

		@Override
		public String getLogin() {
			return this.login;
		}

		public boolean isLogin(String login) {
			return (this.login != null && this.login.equalsIgnoreCase(login));
		}

		public boolean alreadyConnected() {
			return (state != State.WAITING_IDENTIFICATION);
		}

		@Override
		public void disconnect() {
			this.silentlyClose();
			this.server.disconnectUser(this);

		}

		private SocketChannel getSocketChannel() {
			return this.sc;
		}

		@Override
		public void sendLoginResponse(ByteBuffer response, String login, boolean isAuthorized) {

			assert toSendMsgs.size() == 0;
			this.state = isAuthorized ? State.REGISTERED : State.WAITING_IDENTIFICATION;
			this.toSendMsgs.add(response); // TODO: not very good to do it here
			processOut();

			if (isAuthorized)
				this.login = login;
		}

		@Override
		public String getAddressIP() {
			String adress = this.sc.socket().getInetAddress().getHostAddress();
			return adress;
		}

		@Override
		public int getPort() {
			int converstionPort = this.sc.socket().getPort();
			return converstionPort;
		}
	}

	private final int MAX_QUEUE_SIZE; // max messages number in the queue message.
	private final static int MAX_QUEUE_SIZE_DEFAULT = 3; // max messages number in the queue message by default
	static private int BUFFER_SIZE = 1_024;
	static private Logger logger = Logger.getLogger(Server.class.getName());

	private final ServerSocketChannel serverSocketChannel;
	private final Selector selector;
	private final Set<SelectionKey> selectedKeys;
	private final long timeout;
	private final EnumSet<AdminAction> adminActions = EnumSet.noneOf(AdminAction.class);
	private final BlockingQueue<AdminAction> consoleQueue;
	ArrayList<String> disconnectedUser = new ArrayList<>();
	private final AdminServerMgr adminServer = new AdminServerMgr(this);
	private final OutRequestManager outRequestBuilder = new OutRequestManager(this);

	public Server(int port, long timeout, int maxNumMsg) throws IOException, IllegalAccessException {

		if (port <= 0)
			throw new IllegalArgumentException("Port must be positive");

		if (timeout <= 0)
			throw new IllegalArgumentException("Timeout must be positive");

		if (maxNumMsg <= 0)
			throw new IllegalArgumentException("Message max number must be positive");

		serverSocketChannel = ServerSocketChannel.open();
		serverSocketChannel.bind(new InetSocketAddress(port));
		selector = Selector.open();
		selectedKeys = selector.selectedKeys();
		this.timeout = timeout;
		consoleQueue = new LinkedBlockingQueue<>(1);
		this.MAX_QUEUE_SIZE = maxNumMsg;
	}

	public void launch() throws IOException, InterruptedException {
		serverSocketChannel.configureBlocking(false);
		serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
		Set<SelectionKey> selectedKeys = selector.selectedKeys();

		long lastCkTime = System.currentTimeMillis();
		adminServer.start();
		while (!Thread.interrupted()) {
			disconnectedUser.clear();
			selector.select(this.timeout);
			AdminAction action = consoleQueue.poll();

			if (action != null) {
				if (action == AdminAction.SHUTDOWN) {
					this.serverSocketChannel.close();
					break;
				}
				adminActions.add(action);
			}

			long curTime = System.currentTimeMillis();
			long duration = curTime - lastCkTime;
			if (duration > this.timeout) {
				disconnectionsUnactiveUser();
				lastCkTime = curTime;
			}
			processSelectedKeys();

			selectedKeys.clear();
			notifyDisconnectionsUnactiveUser();
		}
	}

	private void disconnectionsUnactiveUser() {
		Set<SelectionKey> selectionKeys = selector.keys();

		for (SelectionKey key : selectionKeys) {
			if (key.isValid()) {
				Context context = (Context) key.attachment();
				if (context != null) {
					if (!context.activeSinceLastTimeoutCheck) {
						String login = context.getLogin();
						if (login != null && login.length() > 0) // already registred
						{
							this.disconnectedUser.add(context.getLogin());
							logger.warning("--> Disconnecting " + login + " unactive !");
						}
						context.silentlyClose();
					} else
						context.activeSinceLastTimeoutCheck = false;
				}

			}
		}
	}

	private void processSelectedKeys() throws IOException {

		for (SelectionKey key : selectedKeys) {
			if (key.isValid()) {

				if (key.isValid() && key.isAcceptable() && !adminActions.contains(AdminAction.STOP_ACCEPT)) {
					doAccept(key);
				}
				try {
					if (key.isValid() && key.isWritable())
						((Context) key.attachment()).doWrite();

					if (key.isValid() && key.isReadable())
						((Context) key.attachment()).doRead();

				} catch (IOException e) {
					logger.log(Level.INFO, "Connection closed with client due to IOException", e);
					silentlyClose(key);
				} catch (Exception e) {
					logger.log(Level.WARNING, "Connection closed because of error occuring during the connection", e);
					silentlyClose(key);
				}
			}
		}

	}

	private void doAccept(SelectionKey key) throws IOException {

		SocketChannel socketChannel = this.serverSocketChannel.accept();
		if (socketChannel != null) {
			socketChannel.configureBlocking(false);
			SelectionKey selectionKey = socketChannel.register(this.selector, SelectionKey.OP_READ);
			selectionKey.attach(new Context(this, selectionKey, this.outRequestBuilder));
		}
	}

	private void silentlyClose(SelectionKey key) {
		Channel sc = (Channel) key.channel();
		try {
			sc.close();
		} catch (IOException e) {
			// ignore exception
		}
	}

	public static void main(String[] args)
			throws NumberFormatException, IOException, InterruptedException, IllegalAccessException {
		if (args.length < 2) {
			usage();
			return;
		}

		int port = Integer.parseInt(args[0]);
		long timeout = Integer.parseInt(args[1]);
		int maxNumMsg = args.length == 3 ? Integer.parseInt(args[2]) : MAX_QUEUE_SIZE_DEFAULT;
		new Server(port, timeout, maxNumMsg).launch();
	}

	private static void usage() {
		System.out.printf(
				"Usage : ServerSumBetter <port> <timeout> <MsgMaxNum>\n\tport\t\t\t:the port number\n\ttimeout (in ms)\t\t:elapse inactive time authorized for a user.\n\tMsgMaxNum\t\t:number of outgoing messages to send can be kept in mermory by user.( By default: %d).\n", MAX_QUEUE_SIZE_DEFAULT);
	}

	/***
	 * Theses methods are here to help understanding the behavior of the selector
	 ***/

	private void notifyDisconnectionsUnactiveUser() {
		for (String login : this.disconnectedUser) {
			ByteBuffer response = NotifyUserBuilder.get(login, StatusCode.S_DIS_U);
			Set<SelectionKey> selectedKeys = selector.keys();

			for (SelectionKey key : selectedKeys) {
				if (key.isValid()) {
					Context context = (Context) key.attachment();
					if (context != null) {
						context.send(response.duplicate());
						logger.info(String.format("---Disconnecting unactive user: '%s'\n", login));
					}
				}
			}

		}
	}

	@Override
	public void broadcast(ByteBuffer messageBuffer, IUserContext sender) {

		Set<SelectionKey> selectedKeys = selector.keys();

		for (SelectionKey key : selectedKeys) {
			if (key.isValid()) {
				Context context = (Context) key.attachment();
				if (context != null && !context.getSocketChannel().equals(((Context) sender).getSocketChannel()))
					context.send(messageBuffer.duplicate());
			}
		}
	}

	@Override
	public boolean existLogin(String login) {

		Set<SelectionKey> selectedKeys = selector.keys();

		for (SelectionKey key : selectedKeys) {
			if (key.isValid()) {
				Context context = (Context) key.attachment();
				if (context != null && context.isLogin(login))
					return true;
			}
		}

		return false;
	}

	private void disconnectUser(IUserContext user) {

		Set<SelectionKey> selectedKeys = selector.keys();

		for (SelectionKey key : selectedKeys) {
			if (key.isValid()) {
				Context context = (Context) key.attachment();
				if (context != null && context.getSocketChannel().equals(((Context) user).getSocketChannel())) {
					key.cancel();
					break;
				}
			}
		}

	}

	@Override
	public List<String> getUsers() {

		ArrayList<String> users = new ArrayList<>();

		Set<SelectionKey> selectedKeys = selector.keys();

		for (SelectionKey key : selectedKeys) {

			Context context = (Context) key.attachment();
			if (context != null) {
				String login = context.getLogin();
				assert !login.isEmpty();
				users.add(login);
			}
		}
		return users;
	}

	@Override
	public void doAction(AdminAction action) throws InterruptedException {
		consoleQueue.put(action);
		this.selector.wakeup();
	}

	@Override
	public Optional<IUserContext> getUser(String login) {

		Set<SelectionKey> selectedKeys = selector.keys();

		for (SelectionKey key : selectedKeys) {
			if (key.isValid()) {
				Context context = (Context) key.attachment();
				if (context != null && context.isLogin(login))
					return Optional.of(context);
			}
		}

		return Optional.empty();
	}
}


