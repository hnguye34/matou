package feng_nguyen.matou.server;

/**
 * This interface provide some methods for the administration service of the server.
 * Theses administration methods, being sensible action, are only visible to authorized component (AdminServerMangaer)   
 *
 */
public interface IServerAdmin extends IServer   {
	enum AdminAction {STOP_ACCEPT, SHUTDOWN  };
	/** Two actions provided: 
	 * Stop accepting new clients
	 * Shutdown the server
	 * @param action to do
	 * @throws InterruptedException : when the process is interrupted by the client..
	 */
	void doAction (AdminAction action) throws InterruptedException;
}
