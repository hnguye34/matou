package feng_nguyen.matou.server;

import java.nio.ByteBuffer;




/**
 * This interface provide methods on a user connection
 *
 */
public interface IUserContext {
	
	/** 
	 * send a message to the user
	 * @param message: message to send
	 */
	void send (ByteBuffer message);
	/**
	 * get the login of the connected user
	 * @return : login Id
	 */
	String getLogin();
	/** 
	 * send the response to the connection request
	 * @param response: the response on the request Id
	 * @param login : the login Id
	 * @param isAuthorized: is accepted to be registered or not
	 */
	void sendLoginResponse(ByteBuffer response, String login, boolean isAuthorized);
	/*
	 *  true : user already passed the login step  
	 *  false: otherwise
	 */
	/** 
	 * check whether the user is already connected
	 * @return true if the user is already registered
	 * false, if not
	 */
	boolean alreadyConnected();
	/**
	 * interrupt the connection
	 */
	void disconnect();
	/**
	 * get the IP address of the user , from the socket
	 * @return IP address
	 */
	String getAddressIP();
	/**
	 * The connection port on the distant machine of the user 
	 * @return : the 
	 */
	int getPort() ;
}
