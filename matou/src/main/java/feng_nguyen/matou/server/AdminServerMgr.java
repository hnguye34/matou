package feng_nguyen.matou.server;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import feng_nguyen.matou.server.IServerAdmin.AdminAction;

/**
 * this class provide a console to make administration tasks: 
 * stop server, log trace, monitoring connecte clients
 *
 */
class AdminServerMgr {
	private final IServerAdmin server;
	private final Thread thread;
	static private Logger logger = Logger.getLogger(AdminServerMgr.class.getName());
	public AdminServerMgr(IServerAdmin server) {
		this.server = server;

		thread = new Thread(() -> {
			readScanner();
		});
	}
	static void printMenu() {
		System.out.println("\t\t\t*** SERVER ADMINISTRATION   ***\n");
		System.out.println("\t\t\t USER     	: show user list \n");
		System.out.println("\t\t\t SHUTDOWN 	: do not accept new user\n");
		System.out.println("\t\t\t STOPALL  	: stop the server\n");
		System.out.println("\t\t\t LOG <level>	: change log level (INFO, WARNING, SEVERE, OFF) \n\n");
		System.out.print("Your choice:");
	}
	private void readScanner() {
		Logger rootLogger = LogManager.getLogManager().getLogger("");
		try (Scanner scanner = new Scanner(System.in)) {
			printMenu();
			
			while (scanner.hasNextLine() && !Thread.interrupted()) {
				Level level = null;
				String line = scanner.nextLine().toUpperCase();
			
				switch (line.toUpperCase()) {
				case "USER":
					List<String> users = server.getUsers() ;
					int count = users.size();
					System.out.printf("\n --- USERS LIST -----\n\n");
					for (String aLogin: users ) 
						System.out.printf("%s:\n", aLogin);
					
					System.out.printf("\nCount:%d.\n", count);
					break;
				case "SHUTDOWN":
					server.doAction(AdminAction.STOP_ACCEPT);
					System.out.printf("-> No other new client accepted from now.\n");
					break;
				case "STOPALL":
					System.out.printf("-> The server is stopping....\n");
					server.doAction(AdminAction.SHUTDOWN);
					return;
				case "HELP":
					printMenu();
					break;
				case "LOG INFO":
					level = Level.INFO;
					break;
				case "LOG WARNING":
					level = Level.WARNING;
					break;
				case "LOG SEVERE":
					level = Level.SEVERE;
					break;
				case "LOG OFF":
					level = Level.OFF;
					break;	
				default:
					break;
				}
				
				if ( level != null) {
					rootLogger.setLevel(level);
					for (Handler h : rootLogger.getHandlers()) {
					    h.setLevel(level);
					}
				}
				printMenu();
			}
		}
		catch (Exception e) {
			logger.severe(e.getMessage());
			return;
		}
	}

	public void start()  {
			this.thread.start();
	}
	public void stop()  {
		this.thread.interrupt();
}
}