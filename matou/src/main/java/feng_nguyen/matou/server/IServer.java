package feng_nguyen.matou.server;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;

/**
 * This interface provide some usual services of the server 
 *
 */
public interface IServer {
	/**
	 * Check whether the login Id already used
	 * @param login to check
	 * @return
	 * true: this login is currently used by a registered client
	 * false: this login is not currently used by a registered client
	 */
	boolean existLogin(String login);
	/**
	 * get the users currently registered on the server 
	 * @return user list
	 */
	List<String> getUsers();
	/**
	 * broadcast a message to all registered users.
	 * @param messageBuffer: message to broadcast
	 * @param fromSender: the sender of the messsage
	 */
	void broadcast(ByteBuffer messageBuffer, IUserContext fromSender);
	/**
	 * get the interface of a given user
	 * @param login: searched login 
	 * @return optional of the user context.
	 * cannot be null the user does not exist (no longer).
	 */
	Optional<IUserContext> getUser(String login);
	

}
