package feng_nguyen.matou.server;

import feng_nguyen.matou.common.PrvCnvRequestStatus;
import feng_nguyen.matou.common.StatusCode;

/**
 * This interface provide some methods to notify a observer that a request has arrived, fully read
 * and so now ready to be processed
 *
 */
public interface IObserver {
	/* 
	 * 
	 */
	/**
	 * dispatch a generic request  sent by user to server
	 * @param string: request content
	 * @param len: length of the content
	 * @param requestType: the type of the request
	 * @param user: the sender of this request
	 */
	void dispatchGenericRequest( String string, int len, StatusCode requestType, IUserContext user)  ;
	/* dispatch a response to an invitation from by user to server
	 * 
	 */
	/**
	 * dispatch the response of a user to an private invitation from another user
	 * @param status: status of the reponses (accept or not..)
	 * @param requester: the user had sent this invitation
	 * @param privatePort: the port used to exchange file. Provided only when  the invitation is accepted
	 * @param authorisationId : the authorization Id to give back on the first connection. Provided only when  the invitation is accepted 
	 * @param user: the sender of this reponses
	 */
	void dispatchPrivConversationResponse (PrvCnvRequestStatus status, String requester, int privatePort, long authorisationId, IUserContext user)  ;
}
