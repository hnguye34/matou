package feng_nguyen.matou.server.outrequestbuilder;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IServer;

 /**
 * the builder builds the message to broadcast. Followed IRC document.
 *
 */
class BroadcastMessageBuilder {
	private final static Charset UTF8 = Charset.forName("UTF-8");
	public static ByteBuffer  get(String message, IServer server , String sender) {
		ByteBuffer bufLogin = UTF8.encode(sender);
		ByteBuffer bufMsg = UTF8.encode(message);

		ByteBuffer response = ByteBuffer.allocate(3 * Integer.BYTES + bufLogin.remaining() + bufMsg.remaining());
		response.putInt(StatusCode.S_MSG_U.getValue());
		response.putInt(bufLogin.remaining());
		response.put(bufLogin);
		response.putInt(bufMsg.remaining());
		response.put(bufMsg);
		response.flip();
		return response;
	}

}
