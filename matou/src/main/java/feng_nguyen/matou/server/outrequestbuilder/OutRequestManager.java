package feng_nguyen.matou.server.outrequestbuilder;

import java.nio.ByteBuffer;
import java.util.Optional;
import java.util.logging.Logger;

import feng_nguyen.matou.common.PrvCnvRequestStatus;
import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;
import feng_nguyen.matou.server.IServer;

/**
 * This class OutRequestManager implement IObserver.
 * After being notified for a new request 
 * OutRequestManager creates the response,and notify the server
 * to send this response.
 * This class OutRequestManager uses appropriate builder to the generate the reponse,
 * following the IRC document.
 *
 */
public class OutRequestManager implements IObserver {

	static private Logger logger = Logger.getLogger(OutRequestManager.class.getName());
	private final IServer server;

	public OutRequestManager(IServer server) {
		this.server = server;
	}

	@Override
	public void dispatchPrivConversationResponse(PrvCnvRequestStatus reponseStatus, String requesterId,
			int privatePort, long authorisationId, IUserContext user) {

		Optional<IUserContext> requesterContext = server.getUser(requesterId);
		requesterContext.ifPresent(requester -> {
			String adressIP = user.getAddressIP();
			
			logger.info(String.format(
					" private conversation reponse (%d) by '%s' with '%s' [autho %d, Ip: '%s'/ ExPort: '%d'].",
					reponseStatus.toInt(), user, requesterId, authorisationId, adressIP, 	privatePort));
			ByteBuffer reponse = TransfPrivCnvResponseBuilder.get(reponseStatus, adressIP, 
					privatePort, authorisationId, user.getLogin());

			requester.send(reponse);
		});

	}

	@Override
	public void dispatchGenericRequest(String text, int len, StatusCode requestType, IUserContext sender) {
		ByteBuffer response = null;

		switch (requestType) {
		case U_LOG_S:

			if (!sender.alreadyConnected()) { // not accept another login nrequest when already logined

				LoginResponseBuilder loginBuilder = new LoginResponseBuilder();
				response = loginBuilder.get(text, this.server);

				sender.sendLoginResponse(response, loginBuilder.getLogin(), loginBuilder.isAuthorized());

				return;
			} else
				sender.disconnect();

			break;
		case U_MSG_S:
			response = BroadcastMessageBuilder.get(text, this.server, sender.getLogin());
			server.broadcast(response, sender);
			break;
		case U_LIS_S:
			response = GivingUserListBuilder.get(this.server);
			sender.send(response);
			break;
		case U_DIS_S:
			response = NotifyUserBuilder.get(sender.getLogin(), StatusCode.S_DIS_U);
			server.broadcast(response, sender);
			break;
		case U_COV_S:
			server.getUser(text).ifPresent(user -> {
				ByteBuffer askcConversationRequest = NotifyUserBuilder.get(sender.getLogin(), StatusCode.S_TCV_U);
				user.send(askcConversationRequest);
			});

			break;
		default:
			break;
		}

	}

}
