package feng_nguyen.matou.server.outrequestbuilder;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

class StringBufBuilder {
	private final static Charset UTF8 = Charset.forName("UTF-8");
	public static  ByteBuffer get(String userName ) {
		
		ByteBuffer bufLogin = UTF8.encode(userName);
		ByteBuffer response = ByteBuffer.allocate( Integer.BYTES + bufLogin.remaining() );
		response.putInt(bufLogin.remaining());
		response.put(bufLogin);
		response.flip();
		return response;
	}
	
}
