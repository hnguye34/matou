package feng_nguyen.matou.server.outrequestbuilder;

import java.nio.ByteBuffer;

import feng_nguyen.matou.common.LoginStatus;
import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IServer;

/**
 * the builder builds the reponse to a login request of the client. Followed IRC document.
 *
 */
class LoginResponseBuilder {
	
	private static final int MAX_LOGIN_LENG = 20;
	private String login ;
	private boolean isAuthorized ;
	public boolean isAuthorized() {
		return isAuthorized;
	}

	public  ByteBuffer get(String login, IServer server ) {
		
		LoginStatus loginStatus = LoginStatus.OK;

		if (login.length() >= MAX_LOGIN_LENG) 
			loginStatus = LoginStatus.ERR_TOOLONG;
		else if (login.contains(" "))
			loginStatus = LoginStatus.ERR_CHAR_FORBID;
		else if (server.existLogin(login))
			loginStatus = LoginStatus.ERR_ALREADY_USED;
		
		ByteBuffer loginResponse = ByteBuffer.allocate(2 * Integer.BYTES);
		loginResponse.putInt(StatusCode.S_LOG_U.getValue());
		loginResponse.putInt(loginStatus.toInt());
		
		loginResponse.flip();
		
		isAuthorized =  loginStatus == LoginStatus.OK;
		this.login = login;
		return loginResponse;
	}
	
	public String getLogin () {
		return login;
	}
	
	
	
}
