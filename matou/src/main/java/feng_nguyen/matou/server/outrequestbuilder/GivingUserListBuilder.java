package feng_nguyen.matou.server.outrequestbuilder;

import java.nio.ByteBuffer;
import java.util.List;

import feng_nguyen.matou.common.LoginStatus;
import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IServer;


/**
 * the builder builds the message list all connected client. Followed IRC document.
 *
 */
class GivingUserListBuilder {
	
	public static ByteBuffer get (IServer server ) {
		
		List<String> users = server.getUsers();
		StringBuilder allLogins = new StringBuilder();
		
		users.forEach( login -> {
			if (allLogins.length() > 0)
				allLogins.append(LoginStatus.SEPARATOR).append(login);
			else
				allLogins.append(login);
		});
		
		ByteBuffer bufUsers = StringBufBuilder.get(allLogins.toString());
		ByteBuffer response = ByteBuffer.allocate( Integer.BYTES + bufUsers.capacity()  );
		response.putInt(StatusCode.S_LIS_U.getValue());
		response.put(bufUsers);
		response.flip();
		return response;
	}

}
