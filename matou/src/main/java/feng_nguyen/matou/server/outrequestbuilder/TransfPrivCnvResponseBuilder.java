package feng_nguyen.matou.server.outrequestbuilder;

import java.nio.ByteBuffer;
import java.util.logging.Logger;

import feng_nguyen.matou.common.PrvCnvRequestStatus;
import feng_nguyen.matou.common.StatusCode;


/**
 * the builder builds the message to transfer a private invitation. Followed IRC document.
 *
 */
class TransfPrivCnvResponseBuilder {
	static private Logger logger = Logger.getLogger(TransfPrivCnvResponseBuilder.class.getName());
	public static ByteBuffer get(PrvCnvRequestStatus reponseStatus, String adressIP, int exchangeFilePort, long authorisationId, String userB ) {
		
		ByteBuffer privConvResponse = null;
		// WARNING: if the other user has disconnected after accepted the conversation, the port could be zero.
		
		if ( reponseStatus == PrvCnvRequestStatus.OK) { 
			logger.info("Sending yes");
			ByteBuffer ipAdress = StringBufBuilder.get(adressIP);
			ByteBuffer bufUserB = StringBufBuilder.get(userB);
			privConvResponse = ByteBuffer.allocate(3 * Integer.BYTES + Long.BYTES + ipAdress.remaining() + bufUserB.remaining());
			privConvResponse.putInt(StatusCode.S_COV_U.getValue());
			privConvResponse.putInt(reponseStatus.toInt());
			privConvResponse.put(bufUserB);
			privConvResponse.put(ipAdress);
			privConvResponse.putInt(exchangeFilePort);
			privConvResponse.putLong(authorisationId);
		
		}
		else {
			logger.info("Sending no");
			privConvResponse = ByteBuffer.allocate(2 * Integer.BYTES);
			privConvResponse.putInt(StatusCode.S_COV_U.getValue());
			privConvResponse.putInt(reponseStatus.toInt());
		}
		
		privConvResponse.flip();
		
		return privConvResponse;
	}
	
	
}
