package feng_nguyen.matou.server.outrequestbuilder;

import java.nio.ByteBuffer;

import feng_nguyen.matou.common.StatusCode;


public class NotifyUserBuilder {
	
	public static ByteBuffer get ( String login, StatusCode requestCode ) {
		
		ByteBuffer bufUser= StringBufBuilder.get(login);
		ByteBuffer response = ByteBuffer.allocate( Integer.BYTES + bufUser.remaining() );
		
		response.putInt(requestCode.getValue());
		response.put(bufUser);
		response.flip();
		return response;
	}

}
