package feng_nguyen.matou.server.inrequestreader;

import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;

import java.nio.ByteBuffer;

 /**
 * reader reads a request with a integer value
 *
 */
class IntReader implements IMessageReader, IObservableReader {
	private static final int UNKNOWN_VALUE = 9999;
	private enum State {
		DONE, WAITING_VALUE
	};

	private final ByteBuffer bbin;
	private State state = State.WAITING_VALUE;
	private int value = UNKNOWN_VALUE;

	 IntReader(ByteBuffer bbin) {
		this.bbin = bbin;
	}

	private ProcessStatus getProcessStatus() {
		ProcessStatus processStatus = ProcessStatus.REFILL;
		switch (state) {
		case DONE:
			processStatus = ProcessStatus.DONE;
			break;
		case WAITING_VALUE:
			processStatus = ProcessStatus.REFILL;
			break;
		default:
			throw new IllegalStateException ("Error occurs when getting the request type.");
		}

		return processStatus;

	}

	@Override
	public ProcessStatus process() {

		if ( state != State.DONE && this.bbin.position() >= Integer.BYTES)
		{
			this.bbin.flip();
			this.value = bbin.getInt();
			this.bbin.compact();
			this.state = State.DONE;
		}

		ProcessStatus procesStatus = getProcessStatus();
		return procesStatus;
	}

	@Override
	public void reset() {
		state = State.WAITING_VALUE;
		value = -UNKNOWN_VALUE;
	}
	


	@Override
	public void notify(StatusCode requestType, IUserContext user, IObserver observer) {
		observer.dispatchGenericRequest("", this.value, requestType, user);
		
	}

	int getValue() {
		return this.value;
	}

	int pollValue () {
		
		int returnValue =  this.value;
		this.reset();
		return returnValue;
	}
}
