package feng_nguyen.matou.server.inrequestreader;

/**
 * This interface provide the method to get the incomming request from a client.
 *
 */


public interface IMessageReader {
	
	/**
	 * Enum ProcessStatus enumerate the state of the reader
	 * DONE: the request is fully read
	 * REFILL: the request is partially read 
	 *
	 */
	public static enum ProcessStatus {DONE,REFILL};

    /**
     *  process the reading of the request
     * @return the status of the reader
     */
    public ProcessStatus process();

    /**
     * reinitialize the reader: the state, intern local variables..
     */
    public void reset();
    
}
