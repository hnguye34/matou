package feng_nguyen.matou.server.inrequestreader;

import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;

/**
 * This interface provide the method to notify a observer when a request is fully reader
 *
 */

public interface IObservableReader {
	 /**
	  * notify the observer 
	 * @param requestType : the type of the request
	 * @param user : the sernder of this request
	 * @param observer : the observer of this request
	 */
	public  void notify( StatusCode requestType, IUserContext user, IObserver observer)  ;
	 
}
