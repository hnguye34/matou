package feng_nguyen.matou.server.inrequestreader;

import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;

import java.nio.ByteBuffer;

 /**
 * reader reads the request type field
 *
 */
class RequestTypeReader implements IMessageReader, IObservableReader {
	private enum State {
		DONE, WAITING_CODE
	};

	private final ByteBuffer bbin;
	private State state = State.WAITING_CODE;
	private StatusCode requestCode;

	 RequestTypeReader(ByteBuffer bbin) {
		this.bbin = bbin;
	}

	private void onArrivedRequestCodeHandle() {
		this.bbin.flip();
		this.requestCode = StatusCode.from(bbin);
		this.bbin.compact();
		this.state = State.DONE;
	}

	private ProcessStatus getProcessStatus() {
		ProcessStatus processStatus = ProcessStatus.REFILL;
		switch (state) {
		case DONE:
			processStatus = ProcessStatus.DONE;
			break;
		case WAITING_CODE:
			processStatus = ProcessStatus.REFILL;
			break;
		default:
			throw new IllegalStateException ("Error occurs when getting the request type.");
		}

		return processStatus;

	}

	@Override
	public ProcessStatus process() {

		if ( state != State.DONE && this.bbin.position() >= Integer.BYTES)
			onArrivedRequestCodeHandle();

		ProcessStatus procesStatus = getProcessStatus();
		return procesStatus;
	}

	@Override
	public void reset() {
		state = State.WAITING_CODE;
		requestCode = null;
	}
	
	public StatusCode getRequestCode() {
		return this.requestCode;
	}

	@Override
	public void notify(StatusCode requestType, IUserContext user, IObserver observer) {
		observer.dispatchGenericRequest("", requestType.getValue(), requestType, user);
		
	}



}
