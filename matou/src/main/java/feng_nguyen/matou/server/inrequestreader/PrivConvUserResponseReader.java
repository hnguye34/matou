package feng_nguyen.matou.server.inrequestreader;

import feng_nguyen.matou.common.PrvCnvRequestStatus;
import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;

import java.nio.ByteBuffer;
import java.util.logging.Logger;


/**
 * reader reads a response to a private invitation 
 *
 */
class PrivConvUserResponseReader implements IMessageReader, IObservableReader {

	static private Logger logger = Logger.getLogger(PrivConvUserResponseReader.class.getName());
	private enum State {
		DONE, WAITING_CODE_REP, WAITING_REQUESTER,  WATING_PORT, WAITING_AUTH_ID
	};

	private final ByteBuffer bbin;
	private State state;
	private PrvCnvRequestStatus reponseCode;
	private String requester;
	private int privatePort;
	private long authorisationId;
	private final IntReader intReader;
	private final StringReader stringReader;

	PrivConvUserResponseReader(ByteBuffer bbin) {
		this.bbin = bbin;
		intReader = new IntReader(bbin);
		stringReader = new StringReader(bbin);
		reset();
	}

	// bbin: in write-mode before and afterward
	public ProcessStatus process() {
		logger.info("wait status");
		if (state == State.WAITING_CODE_REP && intReader.process() == ProcessStatus.DONE) {
			this.reponseCode = PrvCnvRequestStatus.from(intReader.pollValue() );
			this.state = State.WAITING_REQUESTER;
		}
		logger.info ("wait name");
		if (state == State.WAITING_REQUESTER && stringReader.process() == ProcessStatus.DONE) {
			this.requester = stringReader.pollString();
		
			if (this.reponseCode.equals( PrvCnvRequestStatus.OK) ) 
			{
				logger.info (String.format(" OK WAITING PORT:'%s'/%d.\n", requester,reponseCode.toInt()));
				this.state = State.WATING_PORT;
			}
			else 
				this.state = State.DONE;
			logger.info (String.format(" requester:'%s'/%d.\n", requester,reponseCode.toInt()) );
		}
		
		if (state == State.WATING_PORT && intReader.process() == ProcessStatus.DONE) {
			this.privatePort = intReader.pollValue();
			this.state = State.WAITING_AUTH_ID;
			logger.info(String.format(" port:%d.\n", privatePort));
		}
		logger.info("wait id");
		if (this.state == State.WAITING_AUTH_ID && getLong() == ProcessStatus.DONE ) {
			this.state = State.DONE;
		}
		
		if (this.state == State.DONE)
		{
			logger.info("done") ;
			return ProcessStatus.DONE;
		}
		else {
			logger.info("refill");
			return ProcessStatus.REFILL;
		}

	}

	private ProcessStatus getLong() {
		if (this.bbin.position() >= Long.BYTES) {
			this.bbin.flip();
			this.authorisationId = bbin.getLong();
			this.bbin.compact();
			return ProcessStatus.DONE;
		}
		return ProcessStatus.REFILL;
	}

	@Override
	public void reset() {
		state = State.WAITING_CODE_REP;
		privatePort = 0;
		this.authorisationId = 0;
		this.intReader.reset();
		this.stringReader.reset();
		reponseCode = null;
		requester = "";
	}

	
	@Override
	public void notify(StatusCode requestType, IUserContext user, IObserver observer) {
		observer.dispatchPrivConversationResponse(reponseCode, this.requester,  privatePort, authorisationId, user);
	}

}
