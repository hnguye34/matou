package feng_nguyen.matou.server.inrequestreader;

import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * The class RequestReader used by the UserContext to read an create the incoming request from the user
 * When the request is fully read, RequestReader will notify all observer about the request.
 * This  class RequestReader uses appropriate reader to read , following the IRC document.
 *
 */
public class RequestReader implements IMessageReader {
	private enum State {
		DONE, CREATING_READER, PROCESSING_READER
	};
	static private Logger logger = Logger.getLogger(PrivConvUserResponseReader.class.getName());
	private final IUserContext sender;
	private IMessageReader messageReader;
	private final RequestTypeReader requestTypeReader;
	private State state = State.CREATING_READER;
	private final Map<StatusCode, IMessageReader> readers;
	private final IObserver observer;
	private final StringReader stringReader;
	private StatusCode requestType;
	private final PrivConvUserResponseReader privConvUserReponseReader ;

	public RequestReader(ByteBuffer bbin, IUserContext sender, IObserver observer) {
		Objects.requireNonNull(bbin);
		Objects.requireNonNull(sender);
		Objects.requireNonNull(observer);
		this.sender = sender;
		requestTypeReader = new RequestTypeReader(bbin);
		stringReader = new StringReader(bbin);
		privConvUserReponseReader = new PrivConvUserResponseReader(bbin);
		readers = Map.of(StatusCode.U_MSG_S, stringReader, StatusCode.U_LOG_S, stringReader, 
						StatusCode.U_COV_S, stringReader, 
						StatusCode.U_LIS_S, requestTypeReader,
						StatusCode.U_DIS_S, requestTypeReader,
						StatusCode.U_ACV_S, privConvUserReponseReader);
		this.observer = observer;
		reset();
	}

	@Override
	public ProcessStatus process() {

		if (state == State.CREATING_READER)
			onCreatingReaderHandle();

		if (state == State.PROCESSING_READER)
			onProcessingAppropriateReader();

		ProcessStatus processStatus = getProcessStatus();

		return processStatus;

	}

	private void dispatchEvent() {
		logger.info("dispatchEvent");
		if (this.messageReader instanceof IObservableReader)
			((IObservableReader) this.messageReader).notify(requestType, sender, observer);
		
		// evrything published so reset all now
		this.reset();

	}

	private void onCreatingReaderHandle() {
		ProcessStatus typeStatus = requestTypeReader.process();

		if (typeStatus == ProcessStatus.DONE) {
			this.requestType = requestTypeReader.getRequestCode();
			this.messageReader = readers.get(requestType);

			if (messageReader == null)
				throw new IllegalStateException(String.format("---> [ERROR]The request '%d' not supported yet.", requestType.getValue()));

			state = State.PROCESSING_READER;
		}

	}

	private ProcessStatus getProcessStatus() {
		ProcessStatus processStatus = ProcessStatus.REFILL;
		switch (state) {
		case DONE:
			processStatus = ProcessStatus.DONE;
			break;
		case CREATING_READER:
		case PROCESSING_READER:
			processStatus = ProcessStatus.REFILL;
			break;
		default:
			throw new IllegalStateException("Processus State unknown");
		}

		return processStatus;

	}

	private void onProcessingAppropriateReader() {

		assert (state == State.PROCESSING_READER);

		ProcessStatus processStatus = this.messageReader.process();

		if (processStatus == ProcessStatus.DONE) {
			state = State.DONE;
			dispatchEvent();
		}

	}

	@Override
	public void reset() {
		state = State.CREATING_READER;
		readers.forEach((code, reader) -> reader.reset());
		requestType = null;
	}

}
