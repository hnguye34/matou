package feng_nguyen.matou.server.inrequestreader;

import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;
import feng_nguyen.matou.server.inrequestreader.IMessageReader;
import feng_nguyen.matou.server.inrequestreader.IObservableReader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/**
 * reader reads 2 fields : the length of a text(integer) and a text field
 *
 */
class StringReader implements IMessageReader, IObservableReader {

	private enum State {
		DONE, WAITING_INT, WATING_STR
	};

	private final ByteBuffer bbin;
	private final Charset charset;
	private State state;
	private int value;
	private String text;

	 StringReader(ByteBuffer bbin,  String name) {
		this.bbin = bbin;

		charset = Charset.forName(name);
		reset();
	}

	 StringReader(ByteBuffer bbin) {
		this.bbin = bbin;
		charset = Charset.forName("UTF-8");
		reset();
	}

	private void onWaitingIntHandle() {

		if (this.bbin.position() >= Integer.BYTES) {
			this.bbin.flip();
			int valueTmp = bbin.getInt();
			this.bbin.compact();

			if (valueTmp <= 0)
				throw new IllegalStateException(String.format("Value: %d", valueTmp));

			state = State.WATING_STR;
			value = valueTmp;
		}

	}

	private void onWaitingStrHandle() {

		if (this.bbin.position() >= value) {
			this.bbin.flip();
			int originalLimit = this.bbin.limit();
			bbin.limit(value);
			text = charset.decode(this.bbin).toString();
			this.bbin.limit(originalLimit);
			this.bbin.compact();
			this.state = State.DONE;
		}

	}

	// bbin: in write-mode before and afterward
	public ProcessStatus process() {

		if (state == State.WAITING_INT)
			onWaitingIntHandle();

		if (state == State.WATING_STR)
			onWaitingStrHandle();

		if (this.state == State.DONE)
			return ProcessStatus.DONE;
		else
			return ProcessStatus.REFILL;

	}

	@Override
	public void reset() {
		state = State.WAITING_INT;
		text = "";
		value = 0;
	}
	
	
	String pollString() {
		String valToReturn = this.text;
		this.reset();
		return valToReturn;
	}

	@Override
	public void notify(StatusCode requestType, IUserContext user, IObserver observer) {
		observer.dispatchGenericRequest(this.text, this.value, requestType, user);
		
	}

}
