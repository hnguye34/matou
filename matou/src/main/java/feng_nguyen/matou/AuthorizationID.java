package feng_nguyen.matou;

/**
 * An unique authorization id creator. 
 * An authorization ID is made of 13 bits time stamp + 7 bits increasing number
 * @author Yuheng FENG
 */
public class AuthorizationID {
	/**
	 * 7 bits increasing number
	 */
	private static long counter;
	
	public AuthorizationID() {
		/* create a 7-digit random number */
		counter = (long) (Math.random()*9+1) * 100_0000; 
	}
	
	private synchronized void increase() {
		counter = counter - 7;
		if(counter < 100_0000)
			counter = counter + 1000_0000;
	}
	
	/**
	 * Generate an unique authorization id
	 * 
	 * @return authorization id
	 */	
	public synchronized long get() {
		increase();
		long curTime = System.currentTimeMillis()  ;
		curTime += counter;
		//long authoId= Long.parseLong(Long.toString(System.currentTimeMillis()) + Long.toString(counter));
		return curTime ;
	}
	
	/*
	public static void main(String args[]) throws InterruptedException {
		AuthorizationID a = new AuthorizationID();
		System.out.println(a.get());
		Thread.sleep(1000);
		System.out.println(a.get());
		Thread.sleep(2000);
		System.out.println(a.get());
		Thread.sleep(2000);
		System.out.println(a.get());
	}
	*/
}
