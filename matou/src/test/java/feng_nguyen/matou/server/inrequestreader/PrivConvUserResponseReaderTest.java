package feng_nguyen.matou.server.inrequestreader;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import feng_nguyen.matou.common.PrvCnvRequestStatus;
import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;
import feng_nguyen.matou.server.inrequestreader.PrivConvUserResponseReader;
import feng_nguyen.matou.server.inrequestreader.IMessageReader.ProcessStatus;

/**
 * use cases tests of the  PrivConvUserResponseReader class
 *
 */
public class PrivConvUserResponseReaderTest implements  IUserContext, IObserver{
	ByteBuffer bbin = null;
	String userB = "UserB";
	String userA = "UserA";
	String ipAdress = "MyIp";
	int myFilePort = 7778;
	long authoId = 123456789;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		Charset utf8 = Charset.forName("UTF-8");
		ByteBuffer bufRequester = utf8.encode(userA);
		bbin = ByteBuffer.allocate(Integer.BYTES * 3 + Long.BYTES +  bufRequester.remaining());
		
		bbin.putInt(PrvCnvRequestStatus.OK.toInt());
		bbin.putInt(bufRequester.remaining() );
		bbin.put(bufRequester);
		bbin.putInt(myFilePort);
		bbin.putLong(authoId);
	}

	@Test
	public void testPrivConvUserResponseReader() throws Exception {

		setUp();
		PrivConvUserResponseReader privReader = new PrivConvUserResponseReader(bbin);
		
		assertTrue( privReader != null );
	}

	@Test
	public void testProcess() throws Exception {
	
		setUp();
		PrivConvUserResponseReader privReader = new PrivConvUserResponseReader(bbin);
		ProcessStatus status = privReader.process();
		assertTrue( status == ProcessStatus.DONE );
		
	}

	@Test
	public void testReset() throws Exception {
		setUp();
		PrivConvUserResponseReader privReader = new PrivConvUserResponseReader(bbin);
		ProcessStatus status = privReader.process();
		privReader.reset();
		assertTrue( status == ProcessStatus.DONE );
		
	}

	@Test
	public void testNotifyStatusCodeIRequestSenderIObserver() throws Exception {
		setUp();
		PrivConvUserResponseReader privReader = new PrivConvUserResponseReader(bbin);
		ProcessStatus status = privReader.process();
		privReader.notify(StatusCode.U_ACV_S, this, this);
		assertTrue( status == ProcessStatus.DONE );
	}

	@Override
	public void dispatchGenericRequest(String string, int len, StatusCode requestType, IUserContext user) {
		
		
	}

	@Override
	public void dispatchPrivConversationResponse(PrvCnvRequestStatus status, String requester, int fileExchangePort,
			long authorisationId, IUserContext user) {
		assertTrue( status == PrvCnvRequestStatus.OK );
		assertTrue( requester.equals(this.userA));
		assertTrue( fileExchangePort == this.myFilePort);
		assertTrue( authorisationId == this.authoId);
		
	}

	@Override
	public void send(ByteBuffer message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getLogin() {
		// TODO Auto-generated method stub
		return "userB";
	}

	@Override
	public void sendLoginResponse(ByteBuffer response, String login, boolean isAuthorized) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean alreadyConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getAddressIP() {
		// TODO Auto-generated method stub
		return this.ipAdress;
	}

	@Override
	public int getPort() {
		// TODO Auto-generated method stub
		return this.myFilePort;
	}

}
