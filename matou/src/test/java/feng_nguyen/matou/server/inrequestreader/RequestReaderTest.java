package feng_nguyen.matou.server.inrequestreader;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.junit.Test;

import feng_nguyen.matou.common.PrvCnvRequestStatus;
import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;
import feng_nguyen.matou.server.inrequestreader.IMessageReader.ProcessStatus;

/**
 * use-case tests of the RequestReader class
 *
 */
public class RequestReaderTest implements IUserContext, IObserver {

	private Charset utf8 = Charset.forName("UTF-8");
	private String text;
	private StatusCode requestType = null;
	private int len;
	int myFilePort = 7777;
	long authoId = 123456789;
	PrvCnvRequestStatus status;
	String userA = "Tibo";

	@Test
	public void testPublicMessage() {
		String msg = "This is a test of mesage publising";
		ByteBuffer bufMsg = utf8.encode(msg);

		ByteBuffer bbin = ByteBuffer.allocate(Integer.BYTES * 2 + bufMsg.remaining());
		bbin.putInt(StatusCode.U_MSG_S.getValue());
		bbin.putInt(bufMsg.remaining());
		bbin.put(bufMsg);

		RequestReader requestReader = new RequestReader(bbin, this, this);
		ProcessStatus status = requestReader.process();

		// assertTrue (status == ProcessStatus.DONE);
		assertTrue(this.text.equals(msg));
		assertTrue(this.len == msg.length());
	}

	@Test
	public void testAskingPrivateConversation() {
		String userB = "Lucie";
		ByteBuffer bufUserB = utf8.encode(userB);

		ByteBuffer bbin = ByteBuffer.allocate(Integer.BYTES * 2 + bufUserB.remaining());
		bbin.putInt(StatusCode.U_COV_S.getValue());
		bbin.putInt(bufUserB.remaining());
		bbin.put(bufUserB);

		RequestReader requestReader = new RequestReader(bbin, this, this);
		ProcessStatus status = requestReader.process();

		// assertTrue (status == ProcessStatus.DONE);
		assertTrue(this.text.equals(userB));
		assertTrue(this.len == userB.length());
	}

	@Test
	public void testUserAcceptPrivateConversation() {

		int expectedFilePort = 7777;
		int expectedAuthoId = 123456789;
		PrvCnvRequestStatus expectedTestStatus = PrvCnvRequestStatus.OK;
		String testuserA = "Tibo";

		ByteBuffer bufRequester = utf8.encode(userA);
		ByteBuffer bbin = ByteBuffer.allocate(Integer.BYTES * 4 + Long.BYTES + bufRequester.remaining());

		bbin.putInt(StatusCode.U_ACV_S.getValue());
		bbin.putInt(expectedTestStatus.toInt());
		bbin.putInt(bufRequester.remaining());
		bbin.put(bufRequester);
		bbin.putInt(expectedFilePort);
		bbin.putLong(expectedAuthoId);

		RequestReader requestReader = new RequestReader(bbin, this, this);
		requestReader.process();
		
		assertTrue(this.status == expectedTestStatus);
		assertTrue(testuserA.equals(this.text));
		assertTrue(expectedFilePort == this.myFilePort);
		assertTrue(expectedAuthoId == this.authoId);

	}

	@Test
	public void testUserRefusePrivateConversation() {

		PrvCnvRequestStatus expectedTestStatus = PrvCnvRequestStatus.ERR_REFUSED;
		String testuserA = "Tibo";

		ByteBuffer bufRequester = utf8.encode(userA);
		ByteBuffer bbin = ByteBuffer.allocate(Integer.BYTES * 4 + Long.BYTES + bufRequester.remaining());

		bbin.putInt(StatusCode.U_ACV_S.getValue());
		bbin.putInt(expectedTestStatus.toInt());
		bbin.putInt(bufRequester.remaining());
		bbin.put(bufRequester);
	
		RequestReader requestReader = new RequestReader(bbin, this, this);
		requestReader.process();
		
		assertTrue(this.status == expectedTestStatus);
		assertTrue(testuserA.equals(this.text));
		assertTrue(0 == this.myFilePort);
		assertTrue(0 == this.authoId);
	}

	@Test
	public void testTAskingUserList() {

		ByteBuffer bbin =  ByteBuffer.allocate(Integer.BYTES);
		bbin.putInt(StatusCode.U_LIS_S.getValue());
		RequestReader requestReader = new RequestReader(bbin, this, this);
		ProcessStatus status = requestReader.process();
		// assertTrue (status == ProcessStatus.DONE);
		assertTrue(this.requestType == StatusCode.U_LIS_S);

	}

	@Test
	public void testtDisconnection() {

		ByteBuffer bbin = ByteBuffer.allocate(Integer.BYTES);
		bbin.putInt(StatusCode.U_DIS_S.getValue());
		RequestReader requestReader = new RequestReader(bbin, this, this);
		ProcessStatus status = requestReader.process();

		// assertTrue (status == ProcessStatus.DONE);
		assertTrue(this.requestType == StatusCode.U_DIS_S);

	}
	
	@Test(expected=IllegalStateException.class)
	public void testErrorRequestCodeUnknonw() {

		ByteBuffer bbin =  ByteBuffer.allocate(Integer.BYTES);
		bbin.putInt(12345);
		RequestReader requestReader = new RequestReader(bbin, this, this);
		ProcessStatus status = requestReader.process();
	}

	
	// Implementation test
	@Override
	public void dispatchGenericRequest(String string, int len, StatusCode requestType, IUserContext user) {
		this.text = string;
		this.len = len;
		this.requestType = requestType;

	}

	@Override
	public void dispatchPrivConversationResponse(PrvCnvRequestStatus status, String requester, int fileExchangePort,
			long authorisationId, IUserContext user) {
		this.status = status;
		this.text = requester;
		this.myFilePort = fileExchangePort;
		this.authoId = authorisationId;
	}

	@Override
	public void send(ByteBuffer message) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getLogin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void sendLoginResponse(ByteBuffer response, String login, boolean isAuthorized) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean alreadyConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAddressIP() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPort() {
		// TODO Auto-generated method stub
		return 0;
	}

}
