package feng_nguyen.matou.server.inrequestreader;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import org.junit.Test;

import feng_nguyen.matou.common.PrvCnvRequestStatus;
import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IUserContext;

/**
 * use case tests of the  IntReader class
 *
 */
public class IntReaderTest implements  IUserContext, IObserver{
	int receivedValue ;
	StatusCode requestType;
	
	@Test
	public void testDisconnectionRequest() {
		ByteBuffer bbin = ByteBuffer.allocate(Integer.BYTES );
		bbin.putInt(StatusCode.U_DIS_S.getValue());
		IntReader intreader = new IntReader(bbin);
		intreader.process();
		intreader.notify(null, this, this);
		
		assertTrue ( this.receivedValue == StatusCode.U_DIS_S.getValue());
	}



	@Override
	public void dispatchGenericRequest(String text, int value, StatusCode requestType, IUserContext user) {
		this.receivedValue = value;
		
		
	}

	@Override
	public void dispatchPrivConversationResponse(PrvCnvRequestStatus status, String requester, int fileExchangePort,
			long authorisationId, IUserContext user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void send(ByteBuffer message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getLogin() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void sendLoginResponse(ByteBuffer response, String login, boolean isAuthorized) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean alreadyConnected() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getAddressIP() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPort() {
		// TODO Auto-generated method stub
		return 0;
	}

}
