package feng_nguyen.matou.server.outrequestbuilder;

import static org.junit.Assert.*;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import feng_nguyen.matou.common.LoginStatus;
import feng_nguyen.matou.common.PrvCnvRequestStatus;
import feng_nguyen.matou.common.StatusCode;
import feng_nguyen.matou.server.IObserver;
import feng_nguyen.matou.server.IServer;
import feng_nguyen.matou.server.IUserContext;

/**
 * OutRequestManagerTest: uses cases test for the class OutRequestManager
 *
 */
public class OutRequestManagerTest implements IServer, IUserContext {

	private String login ="logintest";
	private final List<String> users = List.of("user1", "User2");
	private final static Charset UTF8 = Charset.forName("UTF-8");
	private final  ByteBuffer bufLogin= UTF8.encode(login);
	private final OutRequestManager outRequestBuilder= new OutRequestManager(this);
	private boolean isAlreadyRegistered = true;
	private boolean isAuthorized ;
	private boolean senderLogin;
	private boolean loginExists = false;
	private String adressIP = "myPrivateIPAddress";
	ByteBuffer response = null;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
		
	}

	@Test
	public void testBroadcasMessage() {
		
		String msg = "testMsgToBroadcast";
		response = null;
		outRequestBuilder.dispatchGenericRequest(msg, msg.length(), StatusCode.U_MSG_S, this);
		
		assertTrue(response != null);
		assertTrue( response.getInt() == StatusCode.S_MSG_U.getValue() );
		int loginLeng = response.getInt();
		assertEquals( loginLeng , bufLogin.limit());
		
		int lim = response.limit();
		response.limit(loginLeng + response.position());
		String receivedlogin = UTF8.decode(response).toString();
		
		assertEquals( receivedlogin, login);
		response.limit(lim);
		
		
		assertEquals( response.getInt() , UTF8.encode(msg).limit());
		assertEquals( UTF8.decode(response).toString(), msg);
	
	}
	
	@Test
	public void testListUserReponse() {
		OutRequestManager outRequestBuilder= new OutRequestManager(this);
	
		response = null;
		outRequestBuilder.dispatchGenericRequest("", 0, StatusCode.U_LIS_S, this);
		
		assertTrue(response != null);
		assertEquals( response.getInt() , StatusCode.S_LIS_U.getValue() );
		
		int listLeng = response.getInt();
		assertEquals( listLeng , 11);
		
		String usersList = UTF8.decode(response).toString();
		assertEquals( usersList , "user1 User2" );
	
	}
	
	@Test
	public void testLoginOKReponse() {
		isAlreadyRegistered = false;
		OutRequestManager outRequestBuilder= new OutRequestManager(this);
	
		response = null;
		outRequestBuilder.dispatchGenericRequest("", 0, StatusCode.U_LOG_S, this);
		
		assertTrue(response != null);
		assertEquals( response.getInt() , StatusCode.S_LOG_U.getValue() );
		assertEquals( response.getInt() , LoginStatus.OK.toInt() );
	}
	
	@Test
	public void testLoginKOAlreadyRegisterdReponse() {
		isAlreadyRegistered = true;
		OutRequestManager outRequestBuilder= new OutRequestManager(this);
	
		response = null;
		outRequestBuilder.dispatchGenericRequest("", 0, StatusCode.U_LOG_S, this);
		
		assertTrue(response == null);
		
	}
	
	@Test
	public void testLoginKOTooLong() {
		
		String loginLong = "LoginTrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrresLong";
		
		
		isAlreadyRegistered = false;
		OutRequestManager outRequestBuilder= new OutRequestManager(this);
	
		response = null;
		outRequestBuilder.dispatchGenericRequest(loginLong, loginLong.length(), StatusCode.U_LOG_S, this);
	
		assertTrue( response != null);
		assertEquals( response.getInt() , StatusCode.S_LOG_U.getValue() );
		assertEquals( response.getInt() , LoginStatus.ERR_TOOLONG.toInt() );
		
	}
	
	
	@Test
	public void testLoginKOwithSpace() {
		
		String loginwithSpace = "ali ce";
		
		
		isAlreadyRegistered = false;
		OutRequestManager outRequestBuilder= new OutRequestManager(this);
	
		response = null;
		outRequestBuilder.dispatchGenericRequest(loginwithSpace, loginwithSpace.length(), StatusCode.U_LOG_S, this);
	
		assertTrue( response != null);
		assertEquals( response.getInt() , StatusCode.S_LOG_U.getValue() );
		assertEquals( response.getInt() , LoginStatus.ERR_CHAR_FORBID.toInt() );
		
	}
	
	@Test
	public void testKOLoginAlreadyExist() {
		
		String loginwithSpace = "alice";
		
		
		isAlreadyRegistered = false;
		loginExists = true;
		OutRequestManager outRequestBuilder= new OutRequestManager(this);
	
		response = null;
		outRequestBuilder.dispatchGenericRequest(loginwithSpace, loginwithSpace.length(), StatusCode.U_LOG_S, this);
	
		assertTrue( response != null);
		assertEquals( response.getInt() , StatusCode.S_LOG_U.getValue() );
		assertEquals( response.getInt() , LoginStatus.ERR_ALREADY_USED.toInt() );
		
	}

	
	@Test
	public void testPrivConversationReponse() {
		
		String loginwithSpace = "alice";
		
		
		isAlreadyRegistered = false;
		loginExists = true;
		OutRequestManager outRequestBuilder= new OutRequestManager(this);
	
		response = null;
		outRequestBuilder.dispatchPrivConversationResponse(PrvCnvRequestStatus.OK, "12", 7777, 1234, this);
		
		assertTrue( response != null);
		assertEquals( response.getInt() , StatusCode.S_COV_U.getValue() );
		assertEquals( response.getInt() , PrvCnvRequestStatus.OK.toInt() );

		int loginLeng = response.getInt();
		assertEquals( loginLeng , bufLogin.limit());
		
		int lim = response.limit();
		response.limit(loginLeng + response.position());
		String receivedlogin = UTF8.decode(response).toString();
		
		assertEquals( receivedlogin , this.login);
		
		response.limit(lim);
		int ipLeng = response.getInt();
		
		assertEquals( ipLeng, UTF8.encode(this.adressIP).remaining());
		
		lim = response.limit();
		response.limit(ipLeng + response.position());
		String receivedIP = UTF8.decode(response).toString();
		response.limit(lim);
		assertEquals( receivedIP , this.adressIP);
		
		assertEquals( response.getInt() , 7777); 
		assertEquals( response.getLong() , 1234); 
		
	}

	
	
	@Override
	public boolean existLogin(String login) {
		return loginExists;
	}

	@Override
	public List<String> getUsers() {
		
		return this.users;
	}

	@Override
	public void broadcast(ByteBuffer messageBuffer, IUserContext fromSender) {
		response = messageBuffer;
		
		
	}

	@Override
	public Optional<IUserContext> getUser(String login) {
		return Optional.of(this);
	}

	@Override
	public void send(ByteBuffer message) {
		this.response = message;
		
	}

	@Override
	public String getLogin() {
		// TODO Auto-generated method stub
		return this.login;
	}

	@Override
	public void sendLoginResponse(ByteBuffer response, String login, boolean isAuthorized) {
		this.response = response ;
		this.isAuthorized = isAuthorized;
		
	}

	@Override
	public boolean alreadyConnected() {
		// TODO Auto-generated method stub
		return isAlreadyRegistered;
	}

	@Override
	public void disconnect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getAddressIP() {
		// TODO Auto-generated method stub
		return this.adressIP;
	}

	@Override
	public int getPort() {
		// TODO Auto-generated method stub
		return 0;
	}

}
